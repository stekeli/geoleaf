package com.example.sifa.geoleaf.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sifa.geoleaf.R;
import com.example.sifa.geoleaf.services.EspeceService;
import com.example.sifa.geoleaf.vo.EspeceAvecNombre;

import java.util.List;

/**
 * Created by Sifa on 06/03/2018.
 */

public class RecensementAdapter extends ArrayAdapter<EspeceAvecNombre> {

    Activity activity;
    List<EspeceAvecNombre> especeAvecNombre;

    public RecensementAdapter(Activity activity, List<EspeceAvecNombre> especeAvecNombre){
        super(activity, -1, especeAvecNombre);
        this.activity = activity;
        this.especeAvecNombre = especeAvecNombre;
    }

    //Evite le chargement à chaque fois des differents recensement
    public static class ViewHolder{
        public TextView nom_francais;
        public TextView nombre;
        public TextView lien_wikipedia;
        public ImageView image;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View rowView = convertView;
        final ViewHolder holder;
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final EspeceAvecNombre eav = especeAvecNombre.get(position);

        //Initialisation des donnees
        if(convertView == null) {
            holder = new ViewHolder();
            rowView = inflater.inflate(R.layout.recensement_adapter, parent, false);

            holder.nom_francais = rowView.findViewById(R.id.nom_francais);
            holder.nombre = rowView.findViewById(R.id.nombre);
            holder.lien_wikipedia = rowView.findViewById(R.id.lien_wikipedia);
            holder.image = rowView.findViewById(R.id.image_espece);

            rowView.setTag(holder);
        }else {
            holder = (ViewHolder) rowView.getTag();
        }

        //widget
        holder.nom_francais.setText(eav.getEspece().getNomFrancais());
        holder.nombre.setText(eav.getNombre().toString() + " fois");
        holder.lien_wikipedia.setText("Wikipédia");

        //chargement image des especes
        new Thread(new Runnable() {
            @Override
            public void run() {
                final Bitmap bitmap = EspeceService.getImage(eav.getEspece(), getContext());
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        holder.image.setBackground(ContextCompat.getDrawable(activity, R.color.colorBlack));
                        holder.image.setImageBitmap(bitmap);
                    }
                });

            }
        }).start();

        //lien wikipedia
        holder.lien_wikipedia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentForWikipedia = new Intent(Intent.ACTION_VIEW, Uri.parse(eav.getEspece().getLien()));
                activity.startActivity(intentForWikipedia);
            }
        });

        return  rowView;
    }

}
