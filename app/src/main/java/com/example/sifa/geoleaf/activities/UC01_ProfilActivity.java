package com.example.sifa.geoleaf.activities;

import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import com.example.sifa.geoleaf.R;
import com.example.sifa.geoleaf.fragments.UC04_InformationsFragment;
import com.example.sifa.geoleaf.fragments.UC05_AlerteFragment;
import com.example.sifa.geoleaf.fragments.UC10_RecensementsFragment;
import com.example.sifa.geoleaf.fragments.UC12_BadgesFragment;

public class UC01_ProfilActivity extends AppCompatActivity {

    public static String FRAGMENT_KEY = "FRAGMENT_ID";

    public final static int ALERTES_FRAGMENT_ID = 0;
    public final static int PROFIL_FRAGMENT_ID = 1;
    public final static int RECENSEMENTS_FRAGMENT_ID = 2;
    public final static int BADGES_FRAGMENT_ID = 3;

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.uc01_activity_profil);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        Integer itemId = getIntent().getIntExtra(FRAGMENT_KEY, 0);
        mViewPager.setCurrentItem(itemId);
    }

    //Gestion des fragments
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if(position == ALERTES_FRAGMENT_ID) {
                return new UC05_AlerteFragment();
            } else if(position == PROFIL_FRAGMENT_ID) {
                return new UC04_InformationsFragment();
            } else if(position == RECENSEMENTS_FRAGMENT_ID){
                return new UC10_RecensementsFragment();
            } else {
                return new UC12_BadgesFragment();
            }
        }

        @Override
        public int getCount() {
            // Show 4 total pages.
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case ALERTES_FRAGMENT_ID:
                    return "Alertes";
                case PROFIL_FRAGMENT_ID:
                    return "Profil";
                case RECENSEMENTS_FRAGMENT_ID:
                    return "Recensements";
                case BADGES_FRAGMENT_ID:
                    return "Badges";
            }
            return null;
        }
    }

    //Bouton précédent en haut à gauche
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
