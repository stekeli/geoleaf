package com.example.sifa.geoleaf.utils;

import android.util.Log;

import com.example.sifa.geoleaf.vo.EspeceVO;
import com.example.sifa.geoleaf.vo.RecensementVO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Sifa on 08/03/2018.
 */

public class RecensementsInstance {

    private static RecensementsInstance instance;
    private int sync = 0;
    private List<RecensementVO> largeData;

    private HashMap<EspeceVO, List<RecensementVO>> notifLargeData = new HashMap<>();


    public synchronized static RecensementsInstance get() {
        if (instance == null) {
            instance = new RecensementsInstance();
        }
        return instance;
    }

    public int setLargeData(List<RecensementVO> largeData) {
        this.largeData = largeData;
        return ++sync;
    }

    public List<RecensementVO> getLargeData(int request) {
        return (request == sync) ? largeData : null;
    }

    public void setNotifLargeData(EspeceVO espece, List<RecensementVO> largeData) {
        notifLargeData.put(espece, largeData);
    }

    public List<RecensementVO> getNotifLargeData(EspeceVO espece) {
        return notifLargeData.get(espece);
    }

}
