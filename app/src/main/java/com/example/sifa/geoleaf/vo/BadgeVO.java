package com.example.sifa.geoleaf.vo;

import java.io.Serializable;

/**
 * Created by Kevin Wallois on 26/01/2018.
 */

public class BadgeVO implements Serializable {
    private int id;
    private String libelle;
    private String description;
    private String imagePath;
    private String type;
    private Integer nombre;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getNombre() {
        return nombre;
    }

    public void setNombre(Integer nombre) {
        this.nombre = nombre;
    }

    @Override
    public boolean equals(Object obj) {
        BadgeVO badge = (BadgeVO) obj;
        return this.getId() == badge.getId();
    }

    @Override
    public int hashCode() {
        return this.getId();
    }
}
