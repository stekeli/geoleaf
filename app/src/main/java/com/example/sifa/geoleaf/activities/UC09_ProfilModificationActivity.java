package com.example.sifa.geoleaf.activities;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.sifa.geoleaf.R;
import com.example.sifa.geoleaf.services.UtilisateurService;
import com.example.sifa.geoleaf.utils.SessionManager;
import com.example.sifa.geoleaf.vo.UtilisateurVO;

public class UC09_ProfilModificationActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText passwordAncien;
    private EditText passwordNouveau;
    private EditText passwordConfirmation;
    private Button mValider;

    private SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.uc09_activity_profil_modification);
        session = new SessionManager(getApplicationContext());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //widget
        passwordAncien = (EditText) findViewById(R.id.idAncienPassword);
        passwordNouveau = (EditText) findViewById(R.id.idNouveauPassword);
        passwordConfirmation = (EditText) findViewById(R.id.idConfirmationPassword);
        mValider = (Button) findViewById(R.id.idBoutonModifierPassword);

        //Listener widget
        mValider.setOnClickListener(this);

    }

    //Listener widgets
    @Override
    public void onClick(View view) {

        switch (view.getId()){

            //Bouton Modifier password
            case R.id.idBoutonModifierPassword:
                if(formulaireOK()) {
                    updatePassword();
                }
                break;

            default:
                break;
        }
    }

    //Mise à jour Mot de passe
    private void updatePassword(){
        new Thread(new Runnable() {
            @Override
            public void run() {

                UtilisateurVO utilisateur = UtilisateurService.getCurrentUtilisateur(getApplicationContext());
                final Boolean passwordCorrect = UtilisateurService.isEmailPasswordExist(utilisateur.getEmail(), passwordAncien.getText().toString());

                // Test ancien password ok
                if(passwordCorrect){

                    //le nouveau password est confirme
                    if(passwordNouveau.getText().toString().equals(passwordConfirmation.getText().toString())){
                        UtilisateurService.updatePassword(utilisateur, passwordNouveau.getText().toString());
                        finish();
                    }
                }else{
                    Snackbar.make(findViewById(android.R.id.content), "Mot de passe actuel incorrect", Snackbar.LENGTH_LONG).show();
                }
            }
        }).start();
    }

    //Password superieur à 4 caracteres
    private boolean isPasswordValid(String pseudo) {
        return pseudo.length() > 4;
    }

    //Validation de surface
    private boolean formulaireOK() {

        String password1 = passwordAncien.getText().toString();
        String password2 = passwordNouveau.getText().toString();
        String password3 = passwordConfirmation.getText().toString();

        // Verification ancien password
        if (TextUtils.isEmpty(password1)) {
            passwordAncien.requestFocus();
            passwordAncien.setError("Votre mot de passe est requis");
            return false;
        } else if (!TextUtils.isEmpty(password1) && !isPasswordValid(password1)) {
            passwordAncien.requestFocus();
            passwordAncien.setError("Le mot de passe est trop court");
            return false;
        }

        // Verification nouveau password
        if (TextUtils.isEmpty(password2)) {
            passwordNouveau.requestFocus();
            passwordNouveau.setError("Le mot de passe est vide");
            return false;
        } else if (!password2.equals(password3)) {
            passwordNouveau.requestFocus();
            passwordNouveau.setError("Le mot de passe de confirmation doit être identique au mot de passe");
            return false;
        }

        return true;
    }

    //Retour activité precedente
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
