package com.example.sifa.geoleaf.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.example.sifa.geoleaf.R;
import com.example.sifa.geoleaf.fragments.UC05_AlerteFragment;
import com.example.sifa.geoleaf.services.AlerteService;
import com.example.sifa.geoleaf.utils.TestConnection;
import com.example.sifa.geoleaf.vo.AlerteVO;

import java.util.List;

/**
 * Created by Kevin Wallois on 06/03/2018.
 */

public class AlerteAdapter extends ArrayAdapter<AlerteVO> {

    Fragment fragment;
    List<AlerteVO> alerteVOs;


    public AlerteAdapter(Fragment fragment, List<AlerteVO> alerteVOs){
        super(fragment.getActivity(), -1, alerteVOs);
        this.fragment = fragment;
        this.alerteVOs = alerteVOs;
    }

    //Evite le chargement à chaque fois des differentes alertes
    public static class ViewHolder{
        public TextView libelle;
        public TextView nomEspece;
        public TextView rayon;
        public Switch actif;
        public ImageView suppression;
    }


    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View rowView = convertView;
        final AlerteAdapter.ViewHolder holder;
        LayoutInflater inflater = (LayoutInflater) fragment.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final AlerteVO alerte = alerteVOs.get(position);

        //Initialisation des donnees
        if(convertView == null) {
            holder = new AlerteAdapter.ViewHolder();
            rowView = inflater.inflate(R.layout.alerte_adapter, parent, false);

            holder.libelle = rowView.findViewById(R.id.idNomAlerte);
            holder.nomEspece = rowView.findViewById(R.id.idNomEspece);
            holder.rayon = rowView.findViewById(R.id.idRayon);
            holder.actif = rowView.findViewById(R.id.idActif);
            holder.suppression = rowView.findViewById(R.id.idCorbeille);

            rowView.setTag(holder);
        }else {
            holder = (AlerteAdapter.ViewHolder) rowView.getTag();
        }

        //Widget
        holder.libelle.setText(alerte.getLibelle());
        holder.nomEspece.setText(alerte.getEspece().getNomFrancais());
        holder.rayon.setText(alerte.getRayon() + " m");
        holder.actif.setChecked(alerte.getActif());

        //listener alerte actif ou non
        holder.actif.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                alerte.setActif(!alerte.getActif());

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        AlerteService.updateAlerte(alerte);
                    }
                }).start();
            }
        });

        //listener corbeille alerte
        holder.suppression.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                //Confirmation dialog
                final AlertDialog.Builder builder = new AlertDialog.Builder(fragment.getActivity());
                builder.setCancelable(true);
                builder.setTitle("Confirmation");
                builder.setMessage("Voulez-vous supprimer " + alerte.getLibelle() + " ?");

                //confirmation
                builder.setPositiveButton("Confirmer", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                //Test si il y a une connexion internet
                                if(TestConnection.isNetworkAvailable(fragment.getActivity())) {
                                    AlerteService.deleteAlerte(alerte);
                                    ((UC05_AlerteFragment) fragment).affichageListeAlerte();
                                }
                            }
                        }).start();
                    }
                });

                //annulation
                builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            }

        });
        return rowView;
    }



}
