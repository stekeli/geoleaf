package com.example.sifa.geoleaf.utils;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

public class Cache {

    private static Cache instance;
    private LruCache<Object, Object> lru;

    private Cache() {

        lru = new LruCache<Object, Object>(1024);

    }

    public static Cache getInstance() {

        if (instance == null) {

            instance = new Cache();
        }

        return instance;

    }

    public LruCache<Object, Object> getLru() {
        return lru;
    }


    public static void put(String key, Bitmap bitmap){
        //Saving bitmap to cache. it will later be retrieved using the bitmap_image key
        Cache.getInstance().getLru().put(key, bitmap);
    }

    public static Bitmap get(String key){
        //To get bitmap from cache using the key. Must cast retrieved cache Object to Bitmap
        Bitmap bitmap = (Bitmap)Cache.getInstance().getLru().get(key);
        return bitmap;
    }

}