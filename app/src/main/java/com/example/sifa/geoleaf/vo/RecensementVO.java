package com.example.sifa.geoleaf.vo;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

/**
 * Created by Kevin Wallois on 26/01/2018.
 */

public class RecensementVO implements Serializable {
    private Integer id;
    private EspeceVO espece;
    private UtilisateurVO utilisateur;
    private Date dateRecensement;
    private Double gpsLatitude;
    private Double gpsLongitude;
    private String imagePath;
    private List<VoteVO> votes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public EspeceVO getEspece() {
        return espece;
    }

    public void setEspece(EspeceVO espece) {
        this.espece = espece;
    }

    public UtilisateurVO getUtilisateur() {
        return utilisateur;
    }

    public void setUtilisateur(UtilisateurVO utilisateur) {
        this.utilisateur = utilisateur;
    }

    public Date getDateRecensement() {
        return dateRecensement;
    }

    public void setDateRecensement(Date dateRecensement) {
        this.dateRecensement = dateRecensement;
    }

    public Double getGpsLatitude() {
        return gpsLatitude;
    }

    public void setGpsLatitude(Double gpsLatitude) {
        this.gpsLatitude = gpsLatitude;
    }

    public Double getGpsLongitude() {
        return gpsLongitude;
    }

    public void setGpsLongitude(Double gpsLongitude) {
        this.gpsLongitude = gpsLongitude;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public List<VoteVO> getVotes() {
        return votes;
    }

    public void setVotes(List<VoteVO> votes) {
        this.votes = votes;
    }

    @Override
    public boolean equals(Object obj) {
      RecensementVO recensement = (RecensementVO) obj;
        return (recensement.getId() == this.getId());
    }

    @Override
    public int hashCode() {
        return this.id;
    }
}
