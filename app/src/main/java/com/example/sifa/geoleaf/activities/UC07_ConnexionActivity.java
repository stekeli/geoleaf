package com.example.sifa.geoleaf.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.sifa.geoleaf.R;
import com.example.sifa.geoleaf.services.UtilisateurService;
import com.example.sifa.geoleaf.utils.SessionManager;
import com.example.sifa.geoleaf.vo.UtilisateurVO;

public class UC07_ConnexionActivity extends AppCompatActivity {

    static final int INSCRIPTION_REQUEST = 2;

    SessionManager session;

    // UI references.
    private EditText mEmail;
    private EditText mPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.uc07_activity_connexion);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        session = new SessionManager(getApplicationContext());

        mEmail = (EditText) findViewById(R.id.email);
        mPassword = (EditText) findViewById(R.id.password);

        Button mConnexionButton = (Button) findViewById(R.id.connexion_button);
        mConnexionButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(inputOK()) authenticate();
            }
        });

        TextView mGotoInscription = (TextView) findViewById(R.id.goto_inscription);
        mGotoInscription.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentForInscription = new Intent(UC07_ConnexionActivity.this, UC08_InscriptionActivity.class);
                startActivityForResult(intentForInscription, INSCRIPTION_REQUEST);
            }
        });

    }

    // Verification de surface des données entrées
    private boolean inputOK() {
        String email = mEmail.getText().toString();
        String password = mPassword.getText().toString();

        // Verifiction de surface pour le mot de passe
        if (TextUtils.isEmpty(password)) {
            mPassword.requestFocus();
            mPassword.setError("Le mot de passe est requis");
            return false;
        } else if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPassword.requestFocus();
            mPassword.setError("Le mot de passe est trop court");
            return false;
        }

        // Verifiction de surface pour l'email
        if (TextUtils.isEmpty(email)) {
            mEmail.requestFocus();
            mEmail.setError("L'email est requis");
            return false;
        } else if (!isEmailValid(email)) {
            mEmail.requestFocus();
            mEmail.setError("Format email invalide");
            return false;
        }
        return true;
    }

    //Verification de surface du format pour l'email
    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    //Verification de surface de la taille du mot de passe
    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

    //Procède à l'authentification
    private void authenticate(){
        final String email = this.mEmail.getText().toString();
        final String password = this.mPassword.getText().toString();

        new Thread(new Runnable() {
            @Override
            public void run() {

                boolean isExist = UtilisateurService.isEmailPasswordExist(email, password);
                if (isExist) {
                    // Si l'utilisateur existe, on va le récupérer
                    UtilisateurVO utilisateur = UtilisateurService.getUtilisateurByEmail(email);

                    // Creation de la session
                    session.createLoginSession(utilisateur.getId(), utilisateur.getPseudo(), utilisateur.getEmail(), utilisateur.getDateInscription());

                    Intent intentToAccueil = getIntent();
                    setResult(RESULT_OK, intentToAccueil);
                    finish();

                } else {
                    // Si identifiants incorrects, on affiche un message
                    Snackbar.make(findViewById(android.R.id.content), "Identifiants incorrects.", Snackbar.LENGTH_LONG).show();
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                    }
                });

            }
        }).start();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == INSCRIPTION_REQUEST && resultCode == RESULT_OK) {
            mEmail.setText(data.getStringExtra("email"));
            mPassword.setText(data.getStringExtra("password"));
        }
    }

    //Bouton précédent en haut à gauche
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}

