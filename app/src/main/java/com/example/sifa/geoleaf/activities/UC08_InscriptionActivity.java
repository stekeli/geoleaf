package com.example.sifa.geoleaf.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.sifa.geoleaf.R;
import com.example.sifa.geoleaf.services.UtilisateurService;
import com.example.sifa.geoleaf.utils.SessionManager;
import com.example.sifa.geoleaf.vo.AlerteVO;
import com.example.sifa.geoleaf.vo.BadgeVO;
import com.example.sifa.geoleaf.vo.RecensementVO;
import com.example.sifa.geoleaf.vo.UtilisateurVO;

import java.util.ArrayList;

public class UC08_InscriptionActivity extends AppCompatActivity {

    SessionManager session;

    // UI references.
    private EditText mPseudo;
    private EditText mEmail;
    private EditText mPassword;
    private EditText mConfirmationPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.uc08_activity_inscription);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mPseudo = (EditText) findViewById(R.id.pseudo);
        mEmail = (EditText) findViewById(R.id.email);
        mPassword = (EditText) findViewById(R.id.password);
        mConfirmationPassword = (EditText) findViewById(R.id.confirmation_password);

        Button mInscriptionButton = (Button) findViewById(R.id.inscription_button);
        mInscriptionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(inputOK()) inscription();
            }
        });
    }

    // Verification de surface des données entrées
    private boolean inputOK() {
        String pseudo = mPseudo.getText().toString();
        String email = mEmail.getText().toString();
        String password = mPassword.getText().toString();
        String confirmationPassword = mConfirmationPassword.getText().toString();

        // Verifiction de surface pour le pseudo
        if (TextUtils.isEmpty(pseudo)) {
            mPseudo.requestFocus();
            mPseudo.setError("Le pseudo est requis");
            return false;
        } else if (!TextUtils.isEmpty(pseudo) && !isPseudoValid(pseudo)) {
            mPseudo.requestFocus();
            mPseudo.setError("Le pseudo est trop court");
            return false;
        }

        // Verifiction de surface pour le mot de passe
        if (TextUtils.isEmpty(password)) {
            mPassword.requestFocus();
            mPassword.setError("Le mot de passe est requis");
            return false;
        } else if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPassword.requestFocus();
            mPassword.setError("Le mot de passe est trop court");
            return false;
        }

        // Verifiction de surface pour le mot de passe
        if (TextUtils.isEmpty(confirmationPassword)) {
            mConfirmationPassword.requestFocus();
            mConfirmationPassword.setError("Le mot de passe de confirmation est requis");
            return false;
        } else if (!TextUtils.isEmpty(confirmationPassword) && !isConfirmationPasswordValid(confirmationPassword)) {
            mConfirmationPassword.requestFocus();
            mConfirmationPassword.setError("Le mot de passe de confirmation est trop court");
            return false;
        } else if (!password.equals(confirmationPassword)) {
            mConfirmationPassword.requestFocus();
            mConfirmationPassword.setError("Le mot de passe de confirmation doit être identique au mot de passe");
            return false;
        }

        // Verifiction de surface pour l'email
        if (TextUtils.isEmpty(email)) {
            mEmail.requestFocus();
            mEmail.setError("L'email est requis");
            return false;
        } else if (!isEmailValid(email)) {
            mEmail.requestFocus();
            mEmail.setError("Format email invalide");
            return false;
        }
        return true;
    }

    //Verification de surface du format pour l'email
    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    //Verification de surface de la taille du mot de passe
    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

    //Verification de surface de la taille du mot de passe de confirmation
    private boolean isConfirmationPasswordValid(String confirmationPassword) {
        return confirmationPassword.length() > 4;
    }
    //Verification de surface de la taille du pseudo
    private boolean isPseudoValid(String pseudo) {
        return pseudo.length() > 4;
    }

    //Procède à l'inscription
    public void inscription(){
        final String pseudo = this.mPseudo.getText().toString();
        final String email = this.mEmail.getText().toString();
        final String password = this.mPassword.getText().toString();

        final UtilisateurVO utilisateur = new UtilisateurVO();
        utilisateur.setPseudo(pseudo);
        utilisateur.setEmail(email);
        utilisateur.setPassword(password);

        new Thread(new Runnable() {
            @Override
            public void run() {

                if (UtilisateurService.isEmailExist(email)) {
                    // Si le pseudo ou l'email existent
                    Snackbar.make(findViewById(android.R.id.content), "L'email " + email + " est déjà utilisé", Snackbar.LENGTH_LONG).show();
                } else {
                    // Verifie si le pseudo existe
                    if (UtilisateurService.isPseudoExist(pseudo)) {
                        // Si le pseudo ou l'email existent déjà
                        Snackbar.make(findViewById(android.R.id.content), "Le pseudo " + pseudo + " est déjà utilisé", Snackbar.LENGTH_LONG).show();
                    } else {
                        UtilisateurService.addUtilisateur(utilisateur);

                        Intent intentToInscription = getIntent();
                        intentToInscription.putExtra("email", utilisateur.getEmail());
                        intentToInscription.putExtra("password", utilisateur.getPassword());
                        setResult(RESULT_OK, intentToInscription);
                        finish();
                    }
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                    }
                });

            }
        }).start();

    }

    //Bouton précédent en haut à gauche
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

}
