package com.example.sifa.geoleaf.cluster;

import android.content.Context;
import android.location.Location;

import com.example.sifa.geoleaf.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

/**
 * Created by Sifa on 23/02/2018.
 */

public class CustomClusterRenderer extends DefaultClusterRenderer<ClusterMarker> {

    private final Context mContext;
    private Gson gson;


    public CustomClusterRenderer(Context context, GoogleMap map, ClusterManager<ClusterMarker> clusterManager) {
        super(context, map, clusterManager);
        mContext = context;
        gson = new GsonBuilder().create();
    }

    @Override
    protected void onBeforeClusterItemRendered(ClusterMarker item, MarkerOptions markerOptions) {
        markerOptions
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker3_resized_fit))
                .title(item.getRecensement().getEspece().getNomFrancais())
                .snippet(gson.toJson(item.getRecensement()));
    }

}
