package com.example.sifa.geoleaf.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.View;

import com.example.sifa.geoleaf.R;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;

/**
 * Created by Sifa on 09/03/2018.
 */

public class TapTargetProvider {

    public static void tapTarget(Activity activity, View view, boolean cancelable, String title, String text) {
        TapTargetView.showFor(activity,
                TapTarget.forView(view, title, text)
                        .cancelable(cancelable)
                        .transparentTarget(true),
                new TapTargetView.Listener() {
                    @Override
                    public void onTargetClick(TapTargetView view) {
                        super.onTargetClick(view);
                    }

                    @Override
                    public void onOuterCircleClick(TapTargetView view) {
                        super.onOuterCircleClick(view);
                    }
                });
    }

}
