package com.example.sifa.geoleaf.activities;

import android.Manifest;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.sifa.geoleaf.R;
import com.example.sifa.geoleaf.dialogs.IdentificationDialog;
import com.example.sifa.geoleaf.services.EspeceService;
import com.example.sifa.geoleaf.services.RecensementService;
import com.example.sifa.geoleaf.utils.PhotoIdentification;
import com.example.sifa.geoleaf.utils.TestConnection;
import com.example.sifa.geoleaf.vo.EspeceVO;
import com.example.sifa.geoleaf.vo.RecensementVO;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;
import java.util.List;

public class UC02_RecenserActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener, IdentificationDialog.IdentificationDialogListener {

    static final int PERMISSION_FINE_LOCATION_REQUEST = 1;
    static final int REQUEST_AUTORISATION = 2;
    static final int REQUEST_TAKE_PHOTO = 3;
    static final int REQUEST_IMAGE_CAPTURE = 4;

    static final int ALPHA_ENABLE = 255;
    static final int ALPHA_DISABLE = 128;

    static final String LABELLE_NOUVEAU_RECEMSEMENT = "nouveauRecensement";

    private RadioGroup radios;
    private RadioButton radio_gps;
    private RadioButton radio_placer;
    private AutoCompleteTextView rechercheEspece;
    private Button recenserButton;
    private ImageView photo;

    private EspeceVO selection;

    private Uri file;
    private GoogleMap map;
    private MapFragment mapFragment;
    private MarkerOptions marker;
    boolean cameraIsMoving = false;
    private LocationManager locManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.uc02_activity_recenser);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        locManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        //widget
        rechercheEspece = (AutoCompleteTextView) findViewById(R.id.idRechercheEspeceRecenser);
        radios = (RadioGroup) findViewById(R.id.radios);
        radio_gps = (RadioButton) findViewById(R.id.radio_gps);
        radio_placer = (RadioButton) findViewById(R.id.radio_placer);
        recenserButton = (Button) findViewById(R.id.recenser_button);
        photo = (ImageView) findViewById(R.id.idImageIdentificationRecenser);

        recenserButton.getBackground().setAlpha(ALPHA_DISABLE);

        //listener widget
        recenserButton.setOnClickListener(this);
        photo.setOnClickListener(this);

        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.recenser_map);
        mapFragment.getMapAsync(this);

        listerEspeces();

        //Clic sur l'espece
        rechercheEspece.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                recenserButton.setEnabled(true);
                recenserButton.getBackground().setAlpha(ALPHA_ENABLE);
                selection = (EspeceVO) adapterView.getItemAtPosition(i);
            }
        });

        //Changement de texte dans la barre de recherche de l'especes
        rechercheEspece.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }


            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    recenserButton.setEnabled(false);
                    recenserButton.getBackground().setAlpha(ALPHA_DISABLE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        //listner bouton radio position
        radios.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                placerMarker(checkedId, false);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mapFragment != null)
            mapFragment.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mapFragment != null)
            mapFragment.onResume();
    }

    //Intent result
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //Prise de la photo
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            idImageIdentificationRecenser();
            recenserButton.setEnabled(true);
            recenserButton.getBackground().setAlpha(ALPHA_ENABLE);
        }

        //Galerie
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            idImageIdentificationRecenser();
            recenserButton.setEnabled(true);
            recenserButton.getBackground().setAlpha(ALPHA_ENABLE);

        }
    }

    //Placement d'un marker
    public void placerMarker(final int checkId, final boolean zoomCamera) throws SecurityException {
        if(getCheckedRadioId() == radio_placer.getId()) {
            FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getApplicationContext());
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // GPS location can be null if GPS is switched off
                            if (location != null) {
                                clearMap();
                                marker = new MarkerOptions()
                                        .position(new LatLng(location.getLatitude(), location.getLongitude()))
                                        .icon(BitmapDescriptorFactory.defaultMarker())
                                        .draggable(true)
                                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                                        .title("Position du recensement");
                                map.addMarker(marker);

                                if(zoomCamera) centerAndZoomCamera();
                                //else centerCamera();
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            e.printStackTrace();
                        }
                    });
        } else {
            map.clear();
        }
        //addLocationListener();
    }

    //Centre la camera sur la position courante
    public void centerCamera(){
        map.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));

    }

    //Centre et zoom la camera sur la position courante
    public void centerAndZoomCamera(){
        cameraIsMoving = true;
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 12.0f));
        cameraIsMoving = false;
    }

    //Retourne la radio cochée
    public int getCheckedRadioId(){
        int checkedId = radios.getCheckedRadioButtonId();
        final RadioButton radioButton = radios.findViewById(checkedId);
        return radioButton.getId();
    }

    //Vide la map
    public void clearMap() {
        map.clear();
    }

    //Recense une espece
    public void recenser() throws SecurityException {
        final RecensementVO recensement = new RecensementVO();
        recensement.setEspece(selection);

        if(getCheckedRadioId() == radio_placer.getId() && marker != null) {
            recensement.setGpsLatitude(Double.valueOf(marker.getPosition().latitude));
            recensement.setGpsLongitude(Double.valueOf(marker.getPosition().longitude));

            new Thread(new Runnable() {
                @Override
                public void run() {
                    final RecensementVO r = RecensementService.addRecensement(recensement, UC02_RecenserActivity.this);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Intent intentForAccueil = getIntent();
                            intentForAccueil.putExtra(LABELLE_NOUVEAU_RECEMSEMENT, r);
                            setResult(RESULT_OK, intentForAccueil);
                            finish();
                        }
                    });
                }
            }).start();

        } else {
            FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getApplicationContext());
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            if (location != null) {
                                recensement.setGpsLatitude(location.getLatitude());
                                recensement.setGpsLongitude(location.getLongitude());

                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        final RecensementVO r = RecensementService.addRecensement(recensement, UC02_RecenserActivity.this);
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Intent intentForAccueil = getIntent();
                                                intentForAccueil.putExtra(LABELLE_NOUVEAU_RECEMSEMENT, r);
                                                setResult(RESULT_OK, intentForAccueil);
                                                finish();
                                            }
                                        });
                                    }
                                }).start();


                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            e.printStackTrace();
                        }
                    });

        }

    }

    //La map est prête
    @Override
    public void onMapReady(final GoogleMap map) {
        //Autorise le zoom sur la map
        map.getUiSettings().setZoomControlsEnabled(true);
        //Active le pointeur sur la position courante
        map.setMyLocationEnabled(true);

        this.map = map;
        if(TestConnection.checkLocationPermission(this)) {
            checkGpsEnabled();
            placerMarker(getCheckedRadioId(), true);
        }else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_FINE_LOCATION_REQUEST);
        }

        map.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) { }

            @Override
            public void onMarkerDrag(Marker m) {
                marker.position(m.getPosition());
            }

            @Override
            public void onMarkerDragEnd(Marker marker) { }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_FINE_LOCATION_REQUEST: { // Si acces GPS autorisé
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    recenser();
                }
                break;
            }
        }
    }

    //Liste toutes les especes
    public void listerEspeces(){

        new Thread(new Runnable() {
            @Override
            public void run() {
                final List<EspeceVO> listeEspeces = new ArrayList<>(EspeceService.getEspeces());
                final ArrayAdapter<EspeceVO> adapter = new ArrayAdapter(UC02_RecenserActivity.this, android.R.layout.simple_list_item_1, listeEspeces);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        rechercheEspece.setThreshold(1);
                        rechercheEspece.setAdapter(adapter);
                    }
                });
            }
        }).start();
    }

    //Verifie l'acces au GPS
    public void checkGpsEnabled(){
        if(!locManager.isProviderEnabled( LocationManager.GPS_PROVIDER )){
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "GPS système inactif.", Snackbar.LENGTH_LONG);
            snackbar.setAction("Activer", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                }
            });
            snackbar.show();
        }
    }

    //Bouton retour en haut à gauche
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    //listener widget
    @Override
    public void onClick(View view) {

        switch (view.getId()){

            //Bouton ajouter un recensement
            case R.id.recenser_button:
                if(TestConnection.checkLocationPermission(getApplicationContext())) {
                    recenser();
                }else {
                    ActivityCompat.requestPermissions(UC02_RecenserActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_FINE_LOCATION_REQUEST);
                }
                break;

            case R.id.idImageIdentificationRecenser:
                //Affiche la dialog
                IdentificationDialog identifier = new IdentificationDialog();
                identifier.show(getFragmentManager(),"identificationDialog");
                break;

            default:
                break;
        }
    }

    //Identifie une espece a partir de la photo
    public void idImageIdentificationRecenser(){

        new Thread(new Runnable() {
            @Override
            public void run() {

                //Recherche espece selon la photo
                final EspeceVO especeVO = EspeceService.getEspeceByImage();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        selection = especeVO;
                        rechercheEspece.setText(especeVO.getNomFrancais());
                        rechercheEspece.dismissDropDown();
                        recenserButton.setEnabled(true);
                        recenserButton.getBackground().setAlpha(ALPHA_ENABLE);
                    }
                });
            }
        }).start();
    }


    //Methode interface acces appareil photo
    @Override
    public void onDialogAppareilPhoto(DialogFragment dialog) {
        file = PhotoIdentification.onDialogAppareilPhoto(dialog, UC02_RecenserActivity.this, REQUEST_AUTORISATION, REQUEST_TAKE_PHOTO);
    }

    //Methode interface acces galerie
    @Override
    public void onDialogGalerie(DialogFragment dialog) {
        PhotoIdentification.onDialogGalerie(dialog, UC02_RecenserActivity.this, REQUEST_IMAGE_CAPTURE);
    }
}
