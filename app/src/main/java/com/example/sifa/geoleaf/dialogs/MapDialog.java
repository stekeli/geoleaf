package com.example.sifa.geoleaf.dialogs;

import android.Manifest;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sifa.geoleaf.R;
import com.example.sifa.geoleaf.activities.UC01_ProfilActivity;
import com.example.sifa.geoleaf.adapters.CustomDetailWindowAdapter;
import com.example.sifa.geoleaf.adapters.CustomInfoWindowAdapter;
import com.example.sifa.geoleaf.cluster.ClusterMarker;
import com.example.sifa.geoleaf.cluster.CustomClusterRenderer;
import com.example.sifa.geoleaf.services.BadgeService;
import com.example.sifa.geoleaf.utils.TestConnection;
import com.example.sifa.geoleaf.vo.BadgeVO;
import com.example.sifa.geoleaf.vo.EspeceVO;
import com.example.sifa.geoleaf.vo.RecensementVO;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.maps.android.clustering.ClusterManager;

import java.io.Serializable;
import java.util.List;

import static com.example.sifa.geoleaf.utils.vars.GEOLEAF_CYCLE;

/**
 * Created by Sifa on 23/02/2018.
 */

public class MapDialog extends DialogFragment implements OnMapReadyCallback {

    static final int PERMISSION_FINE_LOCATION_REQUEST = 1;

    private AlertDialog dialog;
    private EspeceVO espece;
    private List<RecensementVO> recensements;

    private ClusterManager<ClusterMarker> mClusterManager;
    private SupportMapFragment mapFragment;
    private GoogleMap map;

    public static DialogFragment newInstance(EspeceVO espece, List<RecensementVO> recensements) {
        Bundle args = new Bundle();

        args.putSerializable("espece", espece);
        args.putSerializable("recensements", (Serializable) recensements);

        MapDialog fragment = new MapDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.espece = (EspeceVO) getArguments().getSerializable("espece");
        this.recensements = (List<RecensementVO>) getArguments().getSerializable("recensements");
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.map_dialog, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        mapFragment = (SupportMapFragment) getFragmentManager().findFragmentById(R.id.dialog_map);
        mapFragment.getMapAsync(this);

        TextView text = view.findViewById(R.id.dialog_map_text);
        text.setText("Vous avez alerté parce que vous êtes proche de " + recensements.size() + " " + espece.getNomFrancais()+".");

        ImageView quit_imageview = view.findViewById(R.id.quit_imageview);
        quit_imageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        builder.setView(view);
        dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;

        setUpClusterer();

        if(TestConnection.checkLocationPermission(getContext())) {
            doLocation();
        }else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_FINE_LOCATION_REQUEST);
        }

    }

    public void doLocation() throws SecurityException{
/*
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (RecensementVO recensement : recensements) {
            builder.include(new LatLng(recensement.getGpsLatitude(), recensement.getGpsLongitude()));
        }
        LatLngBounds bounds = builder.build();
        int padding = 0; // offset from edges of the map in pixels
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        map.animateCamera(cu);
*/

        FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getContext());
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // GPS location can be null if GPS is switched off
                        if (location != null) {
                            LatLng position = new LatLng(location.getLatitude(), location.getLongitude());
                            map.animateCamera(CameraUpdateFactory.newLatLngZoom(position, 12.0f), new GoogleMap.CancelableCallback() {
                                @Override
                                public void onFinish() {
                                }

                                @Override
                                public void onCancel() {
                                }
                            });
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("MapDemoActivity", "Error trying to get last GPS location");
                        e.printStackTrace();
                    }
                });
    }

    private void setUpClusterer() {
        // Initialize the manager with the context and the map.
        mClusterManager = new ClusterManager(getContext(), map);

        // Ajout des listener de click sur marqueur et fenetre de marqueur
        mClusterManager.getMarkerCollection().setOnInfoWindowAdapter(new CustomInfoWindowAdapter(getContext()));
        mClusterManager.setOnClusterItemInfoWindowClickListener(new CustomDetailWindowAdapter(getContext()));

        map.setInfoWindowAdapter(mClusterManager.getMarkerManager());
        map.setOnInfoWindowClickListener(mClusterManager.getMarkerManager());

        // Rendu du cluster (marker)
        final CustomClusterRenderer renderer = new CustomClusterRenderer(getContext(), map, mClusterManager);
        mClusterManager.setRenderer(renderer);

        // Point the map's listeners at the listeners implemented by the cluster
        // manager.
        final CameraPosition[] mPreviousCameraPosition = {null};
        map.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                CameraPosition position = map.getCameraPosition();
                if(mPreviousCameraPosition[0] == null || mPreviousCameraPosition[0].zoom != position.zoom) {
                    mPreviousCameraPosition[0] = map.getCameraPosition();
                    mClusterManager.cluster();
                }
            }
        });
        map.setOnMarkerClickListener(mClusterManager);

        // Add cluster items (markers) to the cluster manager.
        placeRecensements();
    }

    public void placeRecensements(){
        for(RecensementVO recensement : recensements) {
            ClusterMarker offsetItem = new ClusterMarker(recensement);
            mClusterManager.addItem(offsetItem);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        SupportMapFragment f = (SupportMapFragment) getFragmentManager().findFragmentById(R.id.dialog_map);
        if (f != null)
            getFragmentManager().beginTransaction().remove(f).commit();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_FINE_LOCATION_REQUEST: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    doLocation();
                }
                break;
            }
        }
    }
}
