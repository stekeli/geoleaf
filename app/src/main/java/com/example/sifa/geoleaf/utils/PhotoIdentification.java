package com.example.sifa.geoleaf.utils;

import android.Manifest;
import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Kevin Wallois on 14/03/2018.
 */

public class PhotoIdentification {

    private static Uri file;

    //Appel appareil photo
    public static Uri takePicture(View view, Activity activity, int REQUEST_TAKE_PHOTO) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        file = Uri.fromFile(getOutputMediaFile());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, file);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        activity.startActivityForResult(intent, REQUEST_TAKE_PHOTO);

        return file;
    }

    //récuperation de la photo de l'appareil photo
    private static File getOutputMediaFile(){
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "CameraDemo");

        if (!mediaStorageDir.exists()){
            if (!mediaStorageDir.mkdirs()){
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator + "IMG_"+ timeStamp + ".jpg");
    }

    //Methode interface acces appareil photo
    public static Uri onDialogAppareilPhoto(DialogFragment dialog, Activity activity, int REQUEST_AUTORISATION, int REQUEST_TAKE_PHOTO) {
        //Droit acces appareil photo Identification
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[] { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE }, REQUEST_AUTORISATION);
        }else{
            dialog.dismiss();
            PhotoIdentification.takePicture(activity.getCurrentFocus(), activity, REQUEST_TAKE_PHOTO);
        }
        return file;
    }

    //Methode interface acces galerie
    public static void onDialogGalerie(DialogFragment dialog, Activity activity, int REQUEST_IMAGE_CAPTURE) {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        dialog.dismiss();
        activity.startActivityForResult(pickPhoto , REQUEST_IMAGE_CAPTURE);
    }


}
