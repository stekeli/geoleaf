package com.example.sifa.geoleaf.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sifa.geoleaf.R;
import com.example.sifa.geoleaf.cluster.ClusterMarker;
import com.example.sifa.geoleaf.services.EspeceService;
import com.example.sifa.geoleaf.services.VoteService;
import com.example.sifa.geoleaf.utils.SessionManager;
import com.example.sifa.geoleaf.vo.RecensementVO;
import com.example.sifa.geoleaf.vo.VoteVO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.maps.android.clustering.ClusterManager;

import java.text.SimpleDateFormat;

/**
 * Created by Sifa on 13/02/2018.
 */

public class CustomDetailWindowAdapter extends AppCompatActivity implements ClusterManager.OnClusterItemInfoWindowClickListener<ClusterMarker> {

    private Context context;
    private Gson gson;
    private LayoutInflater inflater;
    private SessionManager session;

    public CustomDetailWindowAdapter(Context context){
        this.gson = new GsonBuilder().create();
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.session = new SessionManager(context.getApplicationContext());
    }


    @Override
    public void onClusterItemInfoWindowClick(ClusterMarker clusterMarker) {
        final View view = inflater.inflate(R.layout.custom_detail_window_adapter, null);
        final AlertDialog.Builder alert = new AlertDialog.Builder(context);

        //boutons vote
        final Button buttonConfirmer = view.findViewById(R.id.button_confirmer);
        final Button buttonRefuter = view.findViewById(R.id.button_refuter);

        alert.setCancelable(true);
        alert.setView(view);
        alert.show();

        //Widget info sur l'espece
        final ImageView imageEspece = view.findViewById(R.id.image_espece);
        TextView nomFrancais = view.findViewById(R.id.nom_francais);
        TextView nomScientifique = view.findViewById(R.id.nom_scientifique);
        TextView dateRecensement = view.findViewById(R.id.date_recensement);
        TextView auteurRecensement = view.findViewById(R.id.auteurRecensement);
        TextView description = view.findViewById(R.id.description);
        TextView lien = view.findViewById(R.id.lien);

        final RecensementVO recensement = clusterMarker.getRecensement();

        //Affichage de la photo de l'espece
        new Thread(new Runnable() {
            @Override
            public void run() {
                final Bitmap bitmap = EspeceService.getImage(recensement.getEspece(), context);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                    imageEspece.setBackground(ContextCompat.getDrawable(context, R.color.colorBlack));
                    imageEspece.setImageBitmap(bitmap);
                    }
                });

            }
        }).start();

        if(session.isLoggedIn()) {
            updateButtonState(buttonConfirmer, buttonRefuter, recensement);

            final VoteVO voteFinal = new VoteVO();

            //Bouton confirmer vote
            buttonConfirmer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    buttonConfirmer.setEnabled(false);
                                    buttonRefuter.setEnabled(false);
                                }
                            });
                            VoteService.addPositiveVote(voteFinal, recensement, context);
                            updateButtonState(buttonConfirmer, buttonRefuter, recensement);
                            updateButtonValues(buttonConfirmer, buttonRefuter, recensement);
                        }
                    }).start();
                }
            });

            //Bouton refuter vote
            buttonRefuter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    buttonConfirmer.setEnabled(false);
                                    buttonRefuter.setEnabled(false);
                                }
                            });
                            VoteService.addNegativeVote(voteFinal, recensement, context);
                            updateButtonState(buttonConfirmer, buttonRefuter, recensement);
                            updateButtonValues(buttonConfirmer, buttonRefuter, recensement);
                        }
                    }).start();
                }
            });
        }

        updateButtonValues(buttonConfirmer, buttonRefuter, recensement);

        //Mise à jour affichage des infos de l'espece
        nomFrancais.setText(recensement.getEspece().getNomFrancais());
        nomScientifique.setText(recensement.getEspece().getNomScientifique());
        description.setText(recensement.getEspece().getDescription());
        dateRecensement.setText("Recensé le " + new SimpleDateFormat("dd MMMM yyyy").format(recensement.getDateRecensement()) + " par");
        auteurRecensement.setText(" " + recensement.getUtilisateur().getPseudo());

        //Listener lien wikipedia
        lien.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentForWikipedia = new Intent(Intent.ACTION_VIEW, Uri.parse(recensement.getEspece().getLien()));
                context.startActivity(intentForWikipedia);
            }
        });
    }

    //Methode gestion des votes confirmation/refutation
    public void updateButtonState(final Button buttonConfirmer, final Button buttonRefuter, final RecensementVO recensement){
        new Thread(new Runnable() {
            @Override
            public void run() {
                final int votedValue = VoteService.getVotedValue(recensement, context);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(votedValue != 0) {
                            buttonConfirmer.setEnabled(false);
                            buttonRefuter.setEnabled(false);
                            if(votedValue == 1) {
                                buttonConfirmer.setBackground(ContextCompat.getDrawable(context, R.drawable.vote_button_confirmation_voted));
                                buttonRefuter.setBackground(ContextCompat.getDrawable(context, R.drawable.vote_button_refutation_voted));
                                buttonRefuter.setText("");
                            } else if(votedValue == -1) {
                                buttonConfirmer.setBackground(ContextCompat.getDrawable(context, R.drawable.vote_button_confirmation_voted));
                                buttonRefuter.setBackground(ContextCompat.getDrawable(context, R.drawable.vote_button_refutation_voted));
                                buttonConfirmer.setText("");
                            }
                        }
                    }
                });
            }
        }).start();
    }

    //Mise a jour du nombre des votes apres avoir voter
    public void updateButtonValues(final Button buttonConfirmer, final Button buttonRefuter, final RecensementVO recensement){
        new Thread(new Runnable() {
            @Override
            public void run() {
                final int negativeVotes = VoteService.countNegativeVotes(recensement);
                final int positiveVotes = VoteService.countPositiveVotes(recensement);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                    buttonRefuter.setText(negativeVotes+"");
                    buttonConfirmer.setText(positiveVotes+"");

                    buttonRefuter.setVisibility(View.VISIBLE);
                    buttonConfirmer.setVisibility(View.VISIBLE);
                    }
                });
            }
        }).start();

    }

}


