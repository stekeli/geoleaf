package com.example.sifa.geoleaf.activities;

import android.app.DialogFragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sifa.geoleaf.R;
import com.example.sifa.geoleaf.dialogs.IdentificationDialog;
import com.example.sifa.geoleaf.services.EspeceService;
import com.example.sifa.geoleaf.utils.PhotoIdentification;
import com.example.sifa.geoleaf.utils.SessionManager;
import com.example.sifa.geoleaf.vo.EspeceVO;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;

public class UC14_IdentificationActivity extends AppCompatActivity implements View.OnClickListener, IdentificationDialog.IdentificationDialogListener{

    //Request pour les intents
    static final int REQUEST_AUTORISATION = 0;
    static final int REQUEST_TAKE_PHOTO = 1;
    static final int REQUEST_IMAGE_CAPTURE = 2;

    //Opacite bouton
    static final int ALPHA_ENABLE = 255;
    static final int ALPHA_DISABLE = 128;

    //widget
    private ImageButton mImageBouton;
    private Button mIdentifier;
    private ImageView photoIdentification;
    private ImageView photoEspece;
    private TextView nomFrancais;
    private TextView nomScientifique;
    private TextView description;

    SessionManager session;
    private Uri file;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.uc14_activity_identification);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        session = new SessionManager(getApplicationContext());

        //Widget
        mImageBouton = (ImageButton) findViewById(R.id.idBoutonCamera);
        mIdentifier = (Button) findViewById(R.id.idBoutonIdentifier);
        photoIdentification = (ImageView) findViewById(R.id.idImageIdentification);
        photoEspece = (ImageView) findViewById(R.id.idImageEspece);
        nomFrancais = (TextView)findViewById(R.id.idNomFrancais);
        nomScientifique = (TextView) findViewById(R.id.idNomScientifique);
        description = (TextView) findViewById(R.id.idDescription);

        mIdentifier.getBackground().setAlpha(ALPHA_DISABLE);

        //listener
        mIdentifier.setOnClickListener(this);
        mImageBouton.setOnClickListener(this);

        int idTapTarget = 1;

        //Tutoriel qui presente comment prendre photo
        if(!session.isVisited(this.getLocalClassName() + idTapTarget)) {
            session.setVisited(this.getLocalClassName() + idTapTarget, true);
            TapTargetView.showFor(this,
                    TapTarget.forView(mImageBouton, "Veuillez selectionner ou prendre une photo").transparentTarget(true),
                    new TapTargetView.Listener() {
                        @Override
                        public void onTargetClick(TapTargetView view) {
                            super.onTargetClick(view);
                            mImageBouton.performClick();
                        }
                    });
            }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            //Bouton camera photoIdentification
            case R.id.idBoutonCamera:

                //Affiche la dialog
                IdentificationDialog identifier = new IdentificationDialog();
                identifier.show(getFragmentManager(),"identificationDialog");
                break;

            //bouton identifier une espece
            case R.id.idBoutonIdentifier:
                identificationImage();
                break;

            default:
                break;
        }

    }

    //Verifications validées
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_AUTORISATION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                file = PhotoIdentification.takePicture(getCurrentFocus(), UC14_IdentificationActivity.this, REQUEST_TAKE_PHOTO);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //Prise de la photo
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            photoIdentification.setImageURI(file);
            mIdentifier.setEnabled(true);
            mIdentifier.getBackground().setAlpha(ALPHA_ENABLE);
        }

        //Galerie
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Uri selectedImage = data.getData();
            photoIdentification.setImageURI(selectedImage);
            mIdentifier.setEnabled(true);
            mIdentifier.getBackground().setAlpha(ALPHA_ENABLE);
        }

        //Tutoriel pour identifier l'espece apres la prise de photo
        if(requestCode == REQUEST_TAKE_PHOTO || requestCode == REQUEST_IMAGE_CAPTURE) {
            int idTapTarget = 2;
            if(!session.isVisited(this.getLocalClassName() + idTapTarget)) {
                session.setVisited(this.getLocalClassName() + idTapTarget, true);
                TapTargetView.showFor(this,
                        TapTarget.forView(mIdentifier, "Cliquez maintenant sur ce bouton pour lancer l'identification").transparentTarget(true),
                        new TapTargetView.Listener() {
                            @Override
                            public void onTargetClick(TapTargetView view) {
                                super.onTargetClick(view);
                                mIdentifier.performClick();
                            }
                        });
            }
        }
    }

    //Identifie une espece a partir de la photo
    public void identificationImage(){

        new Thread(new Runnable() {
            @Override
            public void run() {

                //Stub
                final EspeceVO especeVO = EspeceService.getEspeceByImage();
                final Bitmap bitmap = EspeceService.getImage(especeVO, UC14_IdentificationActivity.this);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        nomFrancais.setText(especeVO.getNomFrancais());
                        nomScientifique.setText(" (" + especeVO.getNomScientifique() + ")");
                        description.setText(especeVO.getDescription());
                        photoEspece.setImageBitmap(bitmap);
                    }
                });

            }
        }).start();
    }

    //Fonctionnalite retour
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    //Methode interface acces appareil photo
    @Override
    public void onDialogAppareilPhoto(DialogFragment dialog) {
        file = PhotoIdentification.onDialogAppareilPhoto(dialog, UC14_IdentificationActivity.this, REQUEST_AUTORISATION, REQUEST_TAKE_PHOTO);
    }

    //Methode interface acces galerie
    @Override
    public void onDialogGalerie(DialogFragment dialog) {
        PhotoIdentification.onDialogGalerie(dialog, UC14_IdentificationActivity.this, REQUEST_IMAGE_CAPTURE);
    }
}


