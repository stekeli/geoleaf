package com.example.sifa.geoleaf.dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.example.sifa.geoleaf.R;

/**
 * Created by Kevin Wallois on 12/03/2018.
 */

public class IdentificationDialog extends DialogFragment implements View.OnClickListener {

    private AlertDialog dialog;
    AlertDialog.Builder builder;
    private IdentificationDialogListener mListener ;

    public interface IdentificationDialogListener {
        void onDialogAppareilPhoto(DialogFragment dialog);
        void onDialogGalerie(DialogFragment dialog);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.identification_dialog, null);

        mListener = (IdentificationDialogListener) view.getContext();

        //Builder
        builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        builder.setTitle("Ajouter une photo");

        //Widget
        TextView appareilPhoto = view.findViewById(R.id.idAppareilPhoto);
        TextView galerie = view.findViewById(R.id.idGalerie);

        //Bouton annuler
        builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        //listener widget
        appareilPhoto.setOnClickListener(this);
        galerie.setOnClickListener(this);

        dialog = builder.create();

        return dialog;
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            //Bouton camera
            case R.id.idAppareilPhoto:
                mListener.onDialogAppareilPhoto(IdentificationDialog.this);
                break;

            //Bouton galerie
            case R.id.idGalerie:
                mListener.onDialogGalerie(IdentificationDialog.this);
                break;
        }
    }

}
