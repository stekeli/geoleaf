package com.example.sifa.geoleaf.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Kevin Wallois on 21/02/2018.
 */

public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("Service Stop", "Erreur BootReceiver");
        context.startService(new Intent(context, NotificationAlerteInit.class));
        context.startService(new Intent(context, NotificationAlerte.class));
    }

}
