package com.example.sifa.geoleaf.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.sifa.geoleaf.R;
import com.example.sifa.geoleaf.services.RecensementService;
import com.example.sifa.geoleaf.utils.RecensementsInstance;
import com.example.sifa.geoleaf.utils.SessionManager;
import com.example.sifa.geoleaf.vo.RecensementVO;

import java.util.List;

public class UC13_LoadingActivity extends AppCompatActivity {

    public static String RECENSEMENTS_KEY = "recensements";

    public static int REQUEST_FEATURES = 1;

    private SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.uc13_activity_loading);

        session = new SessionManager(this); // Gestion de la session

        if(!session.isVisited(this.getLocalClassName())) {
            //Si première visite
            //On affiche les 3 slides de présentation
            Intent intent = new Intent(UC13_LoadingActivity.this, UC14_FeaturesActivity.class);
            startActivityForResult(intent, REQUEST_FEATURES);
            //On enregistre que l'utilisateur a fait sa premiere visite
            session.setVisited(this.getLocalClassName(), true);
        } else {
            //Sinon, on lance la page de chargement puis la page d'accueil
            loadAndLaunchAccueil();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_FEATURES) {
            //Au retour des pages de présentations, on lance la page de chargement puis la page d'accueil
            loadAndLaunchAccueil();
        }
    }

    //Lance le chargement et envoi sur la page d'accueil quand le chargement est terminée
    public void loadAndLaunchAccueil(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                final List<RecensementVO> recensements = RecensementService.getRecensements();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(UC13_LoadingActivity.this, UC00_AccueilActivity.class);
                        int sync = RecensementsInstance.get().setLargeData(recensements);
                        intent.putExtra("recensements", sync);
                        startActivity(intent);
                        finish();
                    }
                });

            }
        }).start();
    }
}
