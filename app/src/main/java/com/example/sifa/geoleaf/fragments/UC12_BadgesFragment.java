package com.example.sifa.geoleaf.fragments;

import android.app.DialogFragment;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.sifa.geoleaf.R;
import com.example.sifa.geoleaf.adapters.BadgeAdapter;
import com.example.sifa.geoleaf.adapters.RecensementAdapter;
import com.example.sifa.geoleaf.dialogs.BadgeDialog;
import com.example.sifa.geoleaf.services.BadgeService;
import com.example.sifa.geoleaf.services.RecensementService;
import com.example.sifa.geoleaf.services.UtilisateurService;
import com.example.sifa.geoleaf.utils.SessionManager;
import com.example.sifa.geoleaf.vo.BadgeAvecAcquis;
import com.example.sifa.geoleaf.vo.BadgeVO;
import com.example.sifa.geoleaf.vo.EspeceAvecNombre;
import com.example.sifa.geoleaf.vo.RecensementVO;
import com.example.sifa.geoleaf.vo.UtilisateurVO;

import java.util.ArrayList;
import java.util.List;

public class UC12_BadgesFragment extends Fragment {

    private ListView listeBadges;

    private SessionManager session;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.uc12_fragment_badges, container, false);

        session = new SessionManager(getActivity());

        listeBadges = view.findViewById(R.id.idListeBadges);

        new Thread(new Runnable() {
            @Override
            public void run() {

                final UtilisateurVO utilisateur = UtilisateurService.getCurrentUtilisateur(getActivity());
                //Va chercher tous les badges
                final List<BadgeVO> badges = BadgeService.getBadges();
                //Va chercher les badges de l'utilisateur
                final List<BadgeVO> user_badges = BadgeService.getBadgesForUtilisateur(utilisateur);

                final List<BadgeAvecAcquis> badgesAvecAcquis = new ArrayList();

                //Crée une liste contenant des badges avec si oui ou non ils sont acquis par l'uitilisateur
                for(BadgeVO badge: badges){
                    Boolean userHasBadge = user_badges.contains(badge);

                    BadgeAvecAcquis baa = new BadgeAvecAcquis();
                    baa.setBadge(badge);
                    baa.setAcquis(userHasBadge);

                    badgesAvecAcquis.add(baa);
                }


                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Affiche
                        listeBadges.setAdapter(new BadgeAdapter(getActivity(), badgesAvecAcquis));
                    }
                });
            }
        }).start();

        return view;
    }



}
