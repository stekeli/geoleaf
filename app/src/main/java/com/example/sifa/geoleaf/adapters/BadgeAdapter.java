package com.example.sifa.geoleaf.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sifa.geoleaf.R;
import com.example.sifa.geoleaf.services.BadgeService;
import com.example.sifa.geoleaf.vo.BadgeAvecAcquis;

import java.util.List;

/**
 * Created by Sifa on 06/03/2018.
 */

public class BadgeAdapter extends ArrayAdapter<BadgeAvecAcquis> {

    Activity activity;
    List<BadgeAvecAcquis> badgeAvecAcquis;

    public BadgeAdapter(Activity activity, List<BadgeAvecAcquis> badgeAvecAcquis){
        super(activity, -1, badgeAvecAcquis);
        this.activity = activity;
        this.badgeAvecAcquis = badgeAvecAcquis;
    }

    //Evite le chargement à chaque fois des differents badges
    public static class ViewHolder{
        public TextView libelle;
        public TextView description;
        public ImageView image_badge;
        public ImageView partager;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final ViewHolder holder;
        BadgeAvecAcquis baa = badgeAvecAcquis.get(position);

        //Initialisation des donnees
        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.badge_adapter, parent, false);

            holder = new ViewHolder();

            holder.libelle = convertView.findViewById(R.id.libelle);
            holder.description = convertView.findViewById(R.id.description);
            holder.image_badge = convertView.findViewById(R.id.image_badge);
            holder.partager = convertView.findViewById(R.id.partager);

            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }

        //widget
        holder.libelle.setText(baa.getBadge().getLibelle());
        holder.description.setText(baa.getBadge().getDescription());
        if(!baa.getAcquis().booleanValue()){
            holder.partager.setVisibility(View.INVISIBLE);
        }

        //Chargement image dans le client
        Bitmap bitmap = BadgeService.getImage(baa.getBadge(), getContext());

        holder.image_badge.setImageBitmap(bitmap);
        if(!baa.getAcquis()) {
            holder.image_badge.setAlpha(100);
        }

        return  convertView;
    }

}
