package com.example.sifa.geoleaf.vo;

import java.sql.Timestamp;

/**
 * Created by Kevin Wallois on 26/01/2018.
 */

public class CriteresRechercheEspeceVO {

    private String ville;
    private String nomEspece;

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getNomEspece() {
        return nomEspece;
    }

    public void setNomEspece(String nomEspece) {
        this.nomEspece = nomEspece;
    }
}
