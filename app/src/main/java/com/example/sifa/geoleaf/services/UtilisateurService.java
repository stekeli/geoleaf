package com.example.sifa.geoleaf.services;

import android.content.Context;

import com.example.sifa.geoleaf.utils.HttpRequest;
import com.example.sifa.geoleaf.utils.SessionManager;
import com.example.sifa.geoleaf.vo.BadgeVO;
import com.example.sifa.geoleaf.vo.UtilisateurBadgeVO;
import com.example.sifa.geoleaf.vo.UtilisateurVO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static com.example.sifa.geoleaf.utils.vars.BASE_URL;

/**
 * Created by Sifa on 31/01/2018.
 */

public class UtilisateurService {

    private static final Gson gson = new GsonBuilder().setDateFormat("yyyy-mm-dd").create();

    public static List<UtilisateurVO> getUtilisateurs() {
        HttpRequest request = HttpRequest.get(BASE_URL + "/utilisateurs");
        Type listUVO = new TypeToken<ArrayList<UtilisateurVO>>(){}.getType();
        List<UtilisateurVO> listeUtilisateurs = gson.fromJson(request.body(), listUVO);
        return listeUtilisateurs;
    }

    //Recuperation de l'utilisateur courrant
    public static UtilisateurVO getCurrentUtilisateur(Context context) {
        SessionManager session = new SessionManager(context);

        HttpRequest request = HttpRequest.get(BASE_URL + "/utilisateur/id", true, "id", session.getId());
        UtilisateurVO utilisateurVO = gson.fromJson(request.body(), UtilisateurVO.class);
        return utilisateurVO;
    }

    //Return UtilisateurVO par son mail
    public static UtilisateurVO getUtilisateurByEmail(String email) {
        HttpRequest request = HttpRequest.get(BASE_URL + "/utilisateur/email", true, "email", email);
        UtilisateurVO utilisateurVO = gson.fromJson(request.body(), UtilisateurVO.class);
        return utilisateurVO;
    }

    public static void addUtilisateur(UtilisateurVO utilisateur) {
        HttpRequest.post(BASE_URL + "/utilisateur/add").acceptJson().contentType("application/json").send(gson.toJson(utilisateur)).code();
    }

    //Mail + Password ok
    public static boolean isEmailPasswordExist(String email, String password) {
        HttpRequest request = HttpRequest.get(BASE_URL + "/utilisateur/exist/email/password", true, "email", email, "password", password);
        return Boolean.parseBoolean(request.body());
    }

    public static boolean isEmailExist(String email) {
        HttpRequest request = HttpRequest.get(BASE_URL + "/utilisateur/exist/email", true, "email", email);
        return Boolean.parseBoolean(request.body());
    }

    public static boolean isPseudoExist(String pseudo) {
        HttpRequest request = HttpRequest.get(BASE_URL + "/utilisateur/exist/pseudo", true, "pseudo", pseudo);
        return Boolean.parseBoolean(request.body());
    }

    //Mise à jour du mot de passe de l'utilisateur
    public static void updatePassword(UtilisateurVO utilisateurVO, String password){
        utilisateurVO.setPassword(password);
        HttpRequest.post(BASE_URL + "/utilisateur/update").acceptJson().contentType("application/json").send(gson.toJson(utilisateurVO)).code();
    }

}
