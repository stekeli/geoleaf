package com.example.sifa.geoleaf.services;

import android.content.Context;

import com.example.sifa.geoleaf.utils.HttpRequest;
import com.example.sifa.geoleaf.vo.RecensementVO;
import com.example.sifa.geoleaf.vo.UtilisateurVO;
import com.example.sifa.geoleaf.vo.VoteVO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static com.example.sifa.geoleaf.utils.vars.BASE_URL;

/**
 * Created by Sifa on 07/02/2018.
 */

public class VoteService {

    private static final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();

    public static List<VoteVO> getVotes() {
        HttpRequest request = HttpRequest.get(BASE_URL + "/votes");
        Type listVVO = new TypeToken<ArrayList<VoteVO>>(){}.getType();
        List<VoteVO> listeVotes = gson.fromJson(request.body(), listVVO);
        return listeVotes;
    }

    public static int getVotedValue(RecensementVO recensement, Context context) {
        UtilisateurVO utilisateur = UtilisateurService.getCurrentUtilisateur(context);
        HttpRequest request = HttpRequest.get(BASE_URL + "/vote/utilisateur/for/recensement", true, "utilisateur", gson.toJson(utilisateur), "recensement", gson.toJson(recensement));
        return Integer.parseInt(request.body());
    }

    public static void addPositiveVote(VoteVO vote, RecensementVO recensement, Context context) {
        vote.setUtilisateur(UtilisateurService.getCurrentUtilisateur(context));
        vote.setRecensement(recensement);
        vote.setNote(1);
        HttpRequest.post(BASE_URL + "/vote/add")
                .acceptJson()
                .contentType("application/json")
                .send(gson.toJson(vote))
                .code();
    }

    public static void addNegativeVote(VoteVO vote, RecensementVO recensement, Context context) {
        vote.setUtilisateur(UtilisateurService.getCurrentUtilisateur(context));
        vote.setRecensement(recensement);
        vote.setNote(-1);
        HttpRequest.post(BASE_URL + "/vote/add").acceptJson().contentType("application/json").send(gson.toJson(vote)).code();
    }

    public static Integer countNegativeVotes(RecensementVO recensement) {
        HttpRequest request = HttpRequest.get(BASE_URL + "/vote/count/negative/recensement", true, "recensement", gson.toJson(recensement));
        Integer count = gson.fromJson(request.body(), Integer.class);
        return count;
    }

    public static Integer countPositiveVotes(RecensementVO recensement) {
        HttpRequest request = HttpRequest.get(BASE_URL + "/vote/count/positive/recensement", true, "recensement", gson.toJson(recensement));
        Integer count = gson.fromJson(request.body(), Integer.class);
        return count;
    }
}
