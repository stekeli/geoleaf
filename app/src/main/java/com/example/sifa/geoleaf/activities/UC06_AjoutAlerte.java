package com.example.sifa.geoleaf.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import com.example.sifa.geoleaf.R;
import com.example.sifa.geoleaf.services.AlerteService;
import com.example.sifa.geoleaf.services.EspeceService;
import com.example.sifa.geoleaf.services.UtilisateurService;
import com.example.sifa.geoleaf.utils.SessionManager;
import com.example.sifa.geoleaf.vo.AlerteVO;
import com.example.sifa.geoleaf.vo.EspeceVO;

import java.util.ArrayList;
import java.util.List;

public class UC06_AjoutAlerte extends AppCompatActivity implements View.OnClickListener{

    //widget
    private Button mAjouter;
    private EditText libelle;
    private EditText rayon;
    private AutoCompleteTextView rechercheEspece;
    private EspeceVO selection;

    private Intent intent;
    SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.uc06_ajout_alerte);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        session = new SessionManager(getApplicationContext());

        //widget
        mAjouter = (Button) findViewById(R.id.idBoutonAjouterAlerte);
        libelle = (EditText) findViewById(R.id.idNomLibelle);
        rayon = (EditText) findViewById(R.id.idRayon);
        rechercheEspece = (AutoCompleteTextView) findViewById(R.id.idRecherche);

        //listener widget
        mAjouter.setOnClickListener(this);

        // Liste Espece pour la barre de recherche
        listerEspeces();

        //clic choix espece
        rechercheEspece.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mAjouter.setEnabled(true);
                selection = (EspeceVO) adapterView.getSelectedItem();
                hideKeyboard(getApplicationContext(),view);
            }
        });

        //listener choix espece
        rechercheEspece.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }


            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length() > 0) {
                    mAjouter.setEnabled(false);
                } else {
                    mAjouter.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }


    //Validation de surface
    private boolean formulaireOK() {

        String nomAlerte = libelle.getText().toString();
        String rayonAlerte = rayon.getText().toString();

        // Verification libelle
        if (TextUtils.isEmpty(nomAlerte)) {
            libelle.requestFocus();
            libelle.setError("Le libellé est obligatoire");
            return false;
        }

        // Verification rayon
        if (TextUtils.isEmpty(rayonAlerte)) {
            rayon.requestFocus();
            rayon.setError("Le rayon est obligatoire");
            return false;
        }
        return true;
    }

    //Permet de cacher le clavier
    public static void hideKeyboard(Context ctx, View v) {
        InputMethodManager imm = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    //Toutes les especes
    public void listerEspeces(){

        new Thread(new Runnable() {
            @Override
            public void run() {
                final List<EspeceVO> listeEspeces = new ArrayList<>(EspeceService.getEspeces());
                final ArrayAdapter<EspeceVO> adapter = new ArrayAdapter(UC06_AjoutAlerte.this, android.R.layout.simple_list_item_1, listeEspeces);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        rechercheEspece.setThreshold(1);
                        rechercheEspece.setAdapter(adapter);
                    }
                });
            }
        }).start();
    }

    //listener widget
    @Override
    public void onClick(View view) {
        switch (view.getId()){

            //ajout alerte
            case R.id.idBoutonAjouterAlerte:
                if(formulaireOK()){
                    ajouterAlerte();
                }
                break;

            default:
                break;
        }
    }

    //Ajoute une alerte
    public void ajouterAlerte(){

        final AlerteVO alerte = new AlerteVO();

        //Renvoie à la liste des alertes
        new Thread(new Runnable() {
            @Override
            public void run() {

                EspeceVO especeVO = EspeceService.getByNomFrancais(rechercheEspece.getText().toString());
                intent = getIntent();

                //Alerte
                alerte.setLibelle(libelle.getText().toString());
                alerte.setRayon(Integer.parseInt(rayon.getText().toString()));
                alerte.setEspece(especeVO);
                alerte.setActif(true);
                alerte.setUtilisateur(UtilisateurService.getCurrentUtilisateur(getApplicationContext()));

                //Renvoie à la liste des alertes
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        alerte.setUtilisateur(UtilisateurService.getCurrentUtilisateur(getApplicationContext()));
                        AlerteService.addAlerte(alerte, getApplicationContext());
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                }).start();
            }
        }).start();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
