package com.example.sifa.geoleaf.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.sifa.geoleaf.R;
import com.example.sifa.geoleaf.activities.UC06_AjoutAlerte;
import com.example.sifa.geoleaf.adapters.AlerteAdapter;
import com.example.sifa.geoleaf.services.AlerteService;
import com.example.sifa.geoleaf.services.UtilisateurService;
import com.example.sifa.geoleaf.utils.SessionManager;
import com.example.sifa.geoleaf.vo.AlerteVO;
import com.example.sifa.geoleaf.vo.UtilisateurVO;

import java.util.List;

import static android.app.Activity.RESULT_OK;

public class UC05_AlerteFragment extends Fragment implements View.OnClickListener{

    //widget
    private ListView listeAlerte;
    private FloatingActionButton ajouterAlerte;
    private LinearLayout tableauLayout;
    private TextView messageListeVide;

    SessionManager session;
    private Intent intent;
    static final int AJOUT_ALERTE_REQUEST = 1;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.uc05_alerte_fragment, container, false);

        session = new SessionManager(getActivity());

        //widget
        listeAlerte = view.findViewById(R.id.idListeAlerte);
        ajouterAlerte = view.findViewById(R.id.idAjoutAlerte);
        tableauLayout = view.findViewById(R.id.idLayoutTableauAlerte);
        messageListeVide = view.findViewById(R.id.idMessagePasAlerte);

        //listener widget
        ajouterAlerte.setOnClickListener(this);

        affichageListeAlerte();

        return view;
    }

    //listener widgets
    @Override
    public void onClick(View view) {

        switch (view.getId()){

            //ajout alerte
            case R.id.idAjoutAlerte:
                intent = new Intent(getActivity(), UC06_AjoutAlerte.class);
                startActivityForResult(intent, AJOUT_ALERTE_REQUEST);
                break;

            default:
                break;
        }
    }

    // Affichage de la liste des Alertes dans la ListView
    public void affichageListeAlerte(){

        new Thread(new Runnable() {
            @Override
            public void run() {

                final UtilisateurVO utilisateur = UtilisateurService.getCurrentUtilisateur(getActivity());
                final List<AlerteVO> alertes = AlerteService.getAlertesByUser(utilisateur);

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        final AlerteAdapter alerteAdapter = new AlerteAdapter(UC05_AlerteFragment.this, alertes);
                        listeAlerte.setAdapter(alerteAdapter);

                        // Liste vide
                        if(alertes.size() == 0 ){
                            tableauLayout.setVisibility(View.INVISIBLE);
                            messageListeVide.setVisibility(View.VISIBLE);
                        }else{
                            tableauLayout.setVisibility(View.VISIBLE);
                            messageListeVide.setVisibility(View.INVISIBLE);
                        }
                    }
                });
            }
        }).start();
    }


    public void onActivityResult(int request, int resultCode, Intent data){
        //Ajout alerte OK
        if(request == AJOUT_ALERTE_REQUEST && resultCode == RESULT_OK){
            affichageListeAlerte();
        }
    }

}
