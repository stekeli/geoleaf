package com.example.sifa.geoleaf.dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sifa.geoleaf.R;
import com.example.sifa.geoleaf.activities.UC01_ProfilActivity;
import com.example.sifa.geoleaf.services.BadgeService;
import com.example.sifa.geoleaf.vo.BadgeVO;

/**
 * Created by Sifa on 23/02/2018.
 */

public class BadgeDialog extends DialogFragment {

    private AlertDialog dialog;
    private BadgeVO badge;

    public static BadgeDialog newInstance(BadgeVO badge) {
        Bundle args = new Bundle();

        args.putSerializable("badge", badge);

        BadgeDialog fragment = new BadgeDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.badge = (BadgeVO) getArguments().getSerializable("badge");
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.badge_dialog, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        TextView libelle = view.findViewById(R.id.libelle);
        TextView description = view.findViewById(R.id.description);
        ImageView image_badge = view.findViewById(R.id.image_badge);

        addListeners(view);

        libelle.setText(badge.getLibelle());
        description.setText(badge.getDescription());
        image_badge.setImageBitmap(BadgeService.getImage(badge, getActivity()));

        builder.setView(view);
        dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        return dialog;
    }

    public void addListeners(View view) {
        Button daccord_button = view.findViewById(R.id.daccord_button);
        Button mesbadges_button = view.findViewById(R.id.mesbadges_button);

        daccord_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        mesbadges_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                Intent intent = new Intent(getActivity(), UC01_ProfilActivity.class);
                intent.putExtra(UC01_ProfilActivity.FRAGMENT_KEY, UC01_ProfilActivity.BADGES_FRAGMENT_ID);
                startActivity(intent);
            }
        });

    }
}
