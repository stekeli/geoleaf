package com.example.sifa.geoleaf.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.util.Log;

import com.example.sifa.geoleaf.activities.UC07_ConnexionActivity;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Sifa on 02/02/2018.
 */

public class SessionManager {
    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    Editor editor;

    // Context
    Context _context;

    private static final String IS_LOGIN = "IsLoggedIn";
    public static final String KEY_ID = "id";
    public static final String KEY_PSEUDO = "pseudo";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_DATE_INSCRIPTION = "date_inscription";
    public static final String KEY_SEEN_ACTIVITIES = "seen_activities";

    public SessionManager(Context context) {
        this._context = context;
        pref = PreferenceManager.getDefaultSharedPreferences(context);
        editor = pref.edit();
    }

    public void createLoginSession(int id, String pseudo, String email, Date date){
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_ID, String.valueOf(id));
        editor.putString(KEY_PSEUDO, pseudo);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_DATE_INSCRIPTION, date.toString());

        editor.commit();
    }

    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_PSEUDO, pref.getString(KEY_ID, null));
        user.put(KEY_PSEUDO, pref.getString(KEY_PSEUDO, null));
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));
        user.put(KEY_DATE_INSCRIPTION, pref.getString(KEY_DATE_INSCRIPTION, null));

        return user;
    }

    public int getId() {
        return Integer.parseInt(pref.getString(KEY_ID, null));
    }

    public String getPseudo() {
        return pref.getString(KEY_PSEUDO, null);
    }

    public String getEmail() {
        return pref.getString(KEY_EMAIL, null);
    }

    public String getDateInscription() {
        return pref.getString(KEY_DATE_INSCRIPTION, null);
    }

    public void checkLogin(){
        // Check login status
        if(!this.isLoggedIn()){
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, UC07_ConnexionActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            _context.startActivity(i);
        }
    }

    public void logoutUser(){
        // Clearing all data from Shared Preferences
        editor.remove(IS_LOGIN);
        editor.remove(KEY_ID);
        editor.remove(KEY_PSEUDO);
        editor.remove(KEY_EMAIL);
        editor.remove(KEY_DATE_INSCRIPTION);

        editor.putBoolean(IS_LOGIN, false);

        editor.commit();
    }

    //Test si l'utilisateur est connecté
    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }

    public void setVisited(String activityName, boolean b) {
        Set<String> activities = pref.getStringSet(KEY_SEEN_ACTIVITIES, new HashSet<String>());
        if(activities != null) {
            if(b) activities.add(activityName);
            else activities.remove(activityName);
            editor.putStringSet(KEY_SEEN_ACTIVITIES, activities);
            editor.commit();
        }
    }

    public boolean isVisited(String activityName) {
        Set<String> activities = pref.getStringSet(KEY_SEEN_ACTIVITIES, new HashSet<String>());
        if(activities == null) {
            return false;
        }
        return activities.contains(activityName);
    }

}