package com.example.sifa.geoleaf.services;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.sifa.geoleaf.dialogs.BadgeDialog;
import com.example.sifa.geoleaf.utils.HttpRequest;
import com.example.sifa.geoleaf.vo.BadgeVO;
import com.example.sifa.geoleaf.vo.EspeceVO;
import com.example.sifa.geoleaf.vo.RecensementVO;
import com.example.sifa.geoleaf.vo.UtilisateurVO;
import com.example.sifa.geoleaf.vo.VoteVO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static com.example.sifa.geoleaf.utils.vars.BASE_URL;

/**
 * Created by Sifa on 07/02/2018.
 */

public class RecensementService {

    private static final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();

    public static List<RecensementVO> getRecensements() {
        HttpRequest request = HttpRequest.get(BASE_URL + "/recensements");
        Type listRVO = new TypeToken<ArrayList<RecensementVO>>(){}.getType();
        List<RecensementVO> listeRecensements = gson.fromJson(request.body(), listRVO);
        return listeRecensements;
    }

    // Retourne la liste des recensements d'une espece
    public static List<RecensementVO> getRecensementsByEspece(EspeceVO especeVO){
        HttpRequest request = HttpRequest.get(BASE_URL + "/recensement/espece" ,true, "espece", gson.toJson(especeVO));
        Type listRVO = new TypeToken<ArrayList<RecensementVO>>(){}.getType();
        List<RecensementVO> listeRecensements = gson.fromJson(request.body(), listRVO);
        return listeRecensements;
    }

    public static RecensementVO getRecensement(Integer id) {
        HttpRequest request = HttpRequest.get(BASE_URL + "/recensement/id", true, "id", id);
        RecensementVO recensement = gson.fromJson(request.body(), RecensementVO.class);
        return recensement;
    }

    public static List<RecensementVO> getRecensementsByUser(UtilisateurVO utilisateur) {
        HttpRequest request = HttpRequest.get(BASE_URL + "/recensements/user", true, "utilisateur", gson.toJson(utilisateur));
        Type listRVO = new TypeToken<ArrayList<RecensementVO>>(){}.getType();
        List<RecensementVO> listeRecensements = gson.fromJson(request.body(), listRVO);
        return listeRecensements;
    }

    public static Integer countRecensementsByUser(UtilisateurVO utilisateur) {
        HttpRequest request = HttpRequest.get(BASE_URL + "/recensements/count/user", true, "utilisateur", gson.toJson(utilisateur));
        Integer nombreRecensements = gson.fromJson(request.body(), Integer.class);
        return nombreRecensements;
    }

    public static RecensementVO addRecensement(RecensementVO recensement, Activity activity) {
        recensement.setUtilisateur(UtilisateurService.getCurrentUtilisateur(activity));
        recensement.setVotes(new ArrayList<VoteVO>());

        HttpRequest request = HttpRequest.post(BASE_URL + "/recensement/add").acceptJson().contentType("application/json").send(gson.toJson(recensement));
        Integer id = Integer.parseInt(request.body());

        return getRecensement(id);
    }

}
