package com.example.sifa.geoleaf.services;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.example.sifa.geoleaf.R;
import com.example.sifa.geoleaf.utils.BitmapSerializer;
import com.example.sifa.geoleaf.utils.Cache;
import com.example.sifa.geoleaf.utils.HttpRequest;
import com.example.sifa.geoleaf.vo.EspeceVO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static com.example.sifa.geoleaf.utils.vars.BASE_URL;

/**
 * Created by Sifa on 06/02/2018.
 */

public class EspeceService {

    private static final Gson gson = new GsonBuilder().setDateFormat("yyyy-mm-dd").create();

    public static List<EspeceVO> getEspeces() {
        HttpRequest request = HttpRequest.get(BASE_URL + "/especes");
        Type listEVO = new TypeToken<ArrayList<EspeceVO>>(){}.getType();
        List<EspeceVO> listeEspeces = gson.fromJson(request.body(), listEVO);
        return listeEspeces;
    }

    public static EspeceVO getByNomFrancais(String nom){
        HttpRequest request = HttpRequest.get(BASE_URL + "/espece/nom", true, "nomFrancais", nom);
        EspeceVO espece = gson.fromJson(request.body(), EspeceVO.class);
        return espece;
    }

    //Retourne une espece selon son id
    public static EspeceVO getById(Integer id){
        HttpRequest request = HttpRequest.get(BASE_URL + "/espece/id", true, "id", id);
        EspeceVO espece = gson.fromJson(request.body(), EspeceVO.class);
        return espece;
    }

    public static Bitmap getImage(EspeceVO espece, Context context) {
        Bitmap bitmap = Cache.get("espece_image"+espece.getId());

        if(bitmap == null) {
            // Si l'image n'est pas dans le cache, on la demande au serveur
            HttpRequest request = HttpRequest.get(BASE_URL + "/espece/image", true, "espece", gson.toJson(espece))/*.header(HttpRequest.HEADER_CONTENT_LENGTH, 10000000)*/;
            bitmap = BitmapSerializer.toBimap(request.body());
            if(bitmap == null) {
                // Si le serveur ne trouve pas l'image, on met l'image d'arbre inconnu
                bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.arbre_inconnu);
            } else {
                // Si le serveur trouve l'image, on la stock dans le cache
                Cache.put("espece_image"+espece.getId()+"", bitmap);
            }
        }

        // On retourne l'image
        return bitmap;
    }


    //Retourne une espece selon l'image
    public static EspeceVO getEspeceByImage(){
        final EspeceVO especeVO = EspeceService.getById(21);
        return especeVO;
    }


}
