package com.example.sifa.geoleaf.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.example.sifa.geoleaf.R;
import com.example.sifa.geoleaf.adapters.RecensementAdapter;
import com.example.sifa.geoleaf.services.RecensementService;
import com.example.sifa.geoleaf.services.UtilisateurService;
import com.example.sifa.geoleaf.utils.SessionManager;
import com.example.sifa.geoleaf.vo.EspeceAvecNombre;
import com.example.sifa.geoleaf.vo.RecensementVO;
import com.example.sifa.geoleaf.vo.UtilisateurVO;

import java.util.ArrayList;
import java.util.List;

public class UC10_RecensementsFragment extends Fragment implements View.OnClickListener{

    //widget
    private ListView listeRecensement;
    private TextView messageListeVide;

    SessionManager session;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.uc10_fragment_recensements, container, false);

        session = new SessionManager(getActivity());

        //widgets
        listeRecensement = view.findViewById(R.id.idListeRecensement);
        messageListeVide = view.findViewById(R.id.idMessagePasRecensements);

        afficherRecensements();

        return view;
    }

    //Affiche les recensements de l'utilisateur
    public void afficherRecensements() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                UtilisateurVO utilisateur = UtilisateurService.getCurrentUtilisateur(getActivity());
                final List<EspeceAvecNombre> listeEspece = new ArrayList<>();

                List<RecensementVO> userRecensements = RecensementService.getRecensementsByUser(utilisateur);

                //Parcourt des recensements
                for(RecensementVO recensement : userRecensements){
                    EspeceAvecNombre eav = new EspeceAvecNombre();
                    eav.setEspece(recensement.getEspece());

                    //Ajoute +1 qu'il a deja referencer l espece
                    if(listeEspece.contains(eav)){
                        int index = listeEspece.indexOf(eav);
                        eav = listeEspece.get(index);
                        eav.setNombre(eav.getNombre()+1);
                        listeEspece.set(index, eav);
                    }else{
                        eav = new EspeceAvecNombre();
                        eav.setEspece(recensement.getEspece());
                        eav.setNombre(1);
                        listeEspece.add(eav);
                    }
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        listeRecensement.setAdapter(new RecensementAdapter(getActivity(), listeEspece));

                        // Liste vide
                        if(listeEspece.size() == 0 ){
                            listeRecensement.setVisibility(View.INVISIBLE);
                            messageListeVide.setVisibility(View.VISIBLE);
                        }else{
                            listeRecensement.setVisibility(View.VISIBLE);
                            messageListeVide.setVisibility(View.INVISIBLE);
                        }
                    }
                });
            }
        }).start();
    }

    //listener widgets
    @Override
    public void onClick(View view) {
        switch (view.getId()){

            default:
                break;
        }
    }

}
