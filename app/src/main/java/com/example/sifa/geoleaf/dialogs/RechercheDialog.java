package com.example.sifa.geoleaf.dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;

import com.example.sifa.geoleaf.R;
import com.example.sifa.geoleaf.services.EspeceService;
import com.example.sifa.geoleaf.vo.EspeceVO;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sifa on 23/02/2018.
 */

public class RechercheDialog extends DialogFragment {

    public interface RechercheDialogListener {
        void onDialogPositiveClick(DialogFragment dialog, EspeceVO especeVO, Place place);
        void onDialogNegativeClick(DialogFragment dialog, EspeceVO especeVO, Place place);
    }

    private RechercheDialogListener mListener;

    private AlertDialog dialog;
    private AutoCompleteTextView autoSearchBarEspece;
    private PlaceAutocompleteFragment autoSearchBarLieu;

    private EspeceVO selectedEspece;
    private Place selectedPlace;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.recherche_dialog, null);
        autoSearchBarEspece = view.findViewById(R.id.auto_search_bar_espece);
        autoSearchBarLieu  = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.auto_search_bar_lieu);
        mListener = (RechercheDialogListener) view.getContext();
        autoSearchBarLieu.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                selectedPlace = place;
            }

            @Override
            public void onError(Status status) { }
        });

        EditText editText = autoSearchBarLieu.getView().findViewById(R.id.place_autocomplete_search_input);
        editText.setHint("Lieu");
        editText.setBackground(autoSearchBarEspece.getBackground());

        ImageButton imageButton = autoSearchBarLieu.getView().findViewById(R.id.place_autocomplete_search_button);
        imageButton.setVisibility(View.GONE);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setView(view)
                .setPositiveButton("Rechercher", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        mListener.onDialogPositiveClick(RechercheDialog.this, selectedEspece, selectedPlace);
                    }
                })
                .setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mListener.onDialogNegativeClick(RechercheDialog.this, selectedEspece, selectedPlace);
                    }
                });
        dialog = builder.create();
        dialog.setTitle("Recherche");


        new Thread(new Runnable() {
            @Override
            public void run() {
                final List<EspeceVO> listeEspeces = new ArrayList<>(EspeceService.getEspeces());
                final ArrayAdapter<EspeceVO> adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, listeEspeces);

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        autoSearchBarEspece.setThreshold(1);
                        autoSearchBarEspece.setAdapter(adapter);
                    }
                });
            }
        }).start();

        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();

        autoSearchBarEspece.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                selectedEspece = (EspeceVO) adapterView.getItemAtPosition(position);
            }
        });

        autoSearchBarEspece.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(charSequence.length() > 0) {
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                } else {
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) { }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        FragmentManager fm = getActivity().getFragmentManager();
        Fragment fragment = (fm.findFragmentById(R.id.auto_search_bar_lieu));
        FragmentTransaction ft = fm.beginTransaction();
        ft.remove(fragment);
        ft.commit();
    }

}
