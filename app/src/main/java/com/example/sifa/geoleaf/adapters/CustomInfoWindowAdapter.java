package com.example.sifa.geoleaf.adapters;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.example.sifa.geoleaf.R;
import com.example.sifa.geoleaf.vo.RecensementVO;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.text.SimpleDateFormat;

/**
 * Created by Sifa on 13/02/2018.
 */

public class CustomInfoWindowAdapter extends AppCompatActivity implements GoogleMap.InfoWindowAdapter {

    private Context context;
    private Gson gson;
    private LayoutInflater inflater;
    private View view;

    public CustomInfoWindowAdapter(final Context context) {
        this.gson = new GsonBuilder().create();
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.view = inflater.inflate(R.layout.custom_info_window_adapter, null);
    }

    @Override
    public View getInfoContents(final Marker marker) {
        final RecensementVO recensement = gson.fromJson(marker.getSnippet(), RecensementVO.class);

        TextView nomFrancais = view.findViewById(R.id.nom_francais);
        TextView dateRecensement = view.findViewById(R.id.date_recensement);

        nomFrancais.setText(recensement.getEspece().getNomFrancais());
        dateRecensement.setText(new SimpleDateFormat("dd MMMM yyyy").format(recensement.getDateRecensement()));

        return view;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

}


