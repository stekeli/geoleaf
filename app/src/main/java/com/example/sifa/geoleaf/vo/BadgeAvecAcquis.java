package com.example.sifa.geoleaf.vo;

/**
 * Created by Sifa on 06/03/2018.
 */

public class BadgeAvecAcquis {

    private BadgeVO badge;
    private Boolean acquis;

    public BadgeVO getBadge() {
        return badge;
    }

    public void setBadge(BadgeVO badge) {
        this.badge = badge;
    }

    public Boolean getAcquis() {
        return acquis;
    }

    public void setAcquis(Boolean acquis) {
        this.acquis = acquis;
    }

    @Override
    public boolean equals(Object obj) {
        BadgeAvecAcquis badgeAvecAcquis = (BadgeAvecAcquis) obj;
        return badge.equals(badgeAvecAcquis.getBadge());
    }

    @Override
    public int hashCode() {
        return badge.hashCode();
    }
}
