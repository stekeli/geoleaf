package com.example.sifa.geoleaf.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sifa.geoleaf.R;
import com.example.sifa.geoleaf.activities.UC09_ProfilModificationActivity;
import com.example.sifa.geoleaf.services.RecensementService;
import com.example.sifa.geoleaf.services.UtilisateurService;
import com.example.sifa.geoleaf.utils.SessionManager;
import com.example.sifa.geoleaf.vo.UtilisateurVO;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UC04_InformationsFragment extends Fragment implements View.OnClickListener{

    SessionManager session;

    private ImageView photoProfil;
    private TextView pseudo;
    private TextView mail;
    private TextView nbEspeceReference;
    private TextView dateInscription;
    private Button mModifiePassword;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.uc04_fragment_informations, container, false);

        session = new SessionManager(getActivity());

        //widgets
        photoProfil = view.findViewById(R.id.idPhotoProfil);
        pseudo = view.findViewById(R.id.idNomPseudo);
        mail = view.findViewById(R.id.idMail);
        nbEspeceReference = view.findViewById(R.id.idNbEspeceReference);
        dateInscription = view.findViewById(R.id.idDateInscription);
        mModifiePassword = view.findViewById(R.id.idModifierPassword);

        //Listener widget
        mModifiePassword.setOnClickListener(this);

        //Mise à jour de l'interface
        new Thread(new Runnable() {
            @Override
            public void run() {
                final UtilisateurVO utilisateur = UtilisateurService.getCurrentUtilisateur(getActivity());
                final Integer nbEspece = RecensementService.countRecensementsByUser(utilisateur);

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Affichage des donnees
                        //TODO : + photo utilisateur
                        pseudo.setText(utilisateur.getPseudo());
                        mail.setText(utilisateur.getEmail());
                        nbEspeceReference.setText(nbEspece.toString());

                        DateFormat df = new SimpleDateFormat("dd MMMM yyyy");
                        Date test = utilisateur.getDateInscription();
                        String reportDate = df.format(test);
                        dateInscription.setText(""+reportDate);

                    }
                });
            }
        }).start();
        return view;
    }

    //listeners widgets
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.idModifierPassword:
                Intent intent_edition = new Intent(getActivity(), UC09_ProfilModificationActivity.class);
                startActivity(intent_edition);
                break;

            default:
                break;
        }
    }

}
