package com.example.sifa.geoleaf.vo;

/**
 * Created by Sifa on 06/03/2018.
 */

public class EspeceAvecNombre {

    private EspeceVO espece;
    private Integer nombre;

    public EspeceVO getEspece() {
        return espece;
    }

    public void setEspece(EspeceVO espece) {
        this.espece = espece;
    }

    public Integer getNombre() {
        return nombre;
    }

    public void setNombre(Integer nombre) {
        this.nombre = nombre;
    }

    @Override
    public boolean equals(Object obj) {
        EspeceAvecNombre obj_espece = (EspeceAvecNombre) obj;
        return espece.equals(obj_espece.getEspece());
    }

    @Override
    public int hashCode() {
        return espece.hashCode();
    }
}
