package com.example.sifa.geoleaf.activities;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.sifa.geoleaf.R;
import com.example.sifa.geoleaf.services.AlerteService;
import com.example.sifa.geoleaf.services.UtilisateurService;
import com.example.sifa.geoleaf.utils.RecensementsInstance;
import com.example.sifa.geoleaf.utils.SessionManager;
import com.example.sifa.geoleaf.utils.TestConnection;
import com.example.sifa.geoleaf.vo.AlerteVO;
import com.example.sifa.geoleaf.vo.EspeceVO;
import com.example.sifa.geoleaf.vo.RecensementVO;
import com.example.sifa.geoleaf.vo.UtilisateurVO;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Kevin Wallois on 21/02/2018.
 */

public class NotificationAlerte extends Service {

    static final int PERMISSION_FINE_LOCATION_REQUEST = 1;

    private Timer mTimer;
    private SessionManager session;

    static List<RecensementVO> listeNotificationEffectue = new ArrayList<>();
    static List<RecensementVO> listeRecensement = new ArrayList<>();
    static List<AlerteVO> listeAlerte = new ArrayList<>();
    HashMap<EspeceVO, List<RecensementVO>> notif = new HashMap<>();


    //duree raffraichissement en seconde
    static final int DUREE_SECONDE = 10;

    TimerTask timerTask = new TimerTask() {
        @Override
        public void run() {
            Log.e("NotificationAlerte","Run TimerTask");
            notifiy();
        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        try{
            mTimer.cancel();
            timerTask.cancel();
        }catch (Exception e){
            e.printStackTrace();
        }
        Intent intent = new Intent("com.example.sifa.geoleaf");
        intent.putExtra("intent","BodyIntent");
        sendBroadcast(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mTimer = new Timer();
        mTimer.schedule(timerTask, 2000, DUREE_SECONDE * 1000);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try{

        }catch (Exception e){
            e.printStackTrace();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    //Creation des notifications des especes à proximités
    public void notifiy() throws SecurityException{


        session = new SessionManager(getApplicationContext());

        if(session.isLoggedIn()) {

            //Test si il y a une connexion internet
            if(TestConnection.isNetworkAvailable(this)) {
                UtilisateurVO utilisateur = UtilisateurService.getCurrentUtilisateur(getApplicationContext());
                listeAlerte = AlerteService.getAlertesByUserActif(utilisateur);

                if (!listeAlerte.isEmpty()) {
                    FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
                    mFusedLocationClient.getLastLocation()
                            .addOnSuccessListener(new OnSuccessListener<Location>() {
                                @Override
                                public void onSuccess(Location location2) {
                                    //localisation utilisateur
                                    Location loc = new Location(location2);

                                    int nbEspeceRayon = 0; //Nb espece dans le rayon (alerte)
                                    HashMap<RecensementVO,Location> loca = new HashMap<>();
                                    HashMap<HashMap<RecensementVO, EspeceVO>, Integer> listeNotification = new HashMap<>();

                                    //Stocke les especes dans une list de Location
                                    for (RecensementVO recensement : listeRecensement) {
                                        Location location = new Location("" + recensement.getEspece().getId());
                                        location.setAltitude(0);
                                        location.setLatitude(recensement.getGpsLatitude());
                                        location.setLongitude(recensement.getGpsLongitude());
                                        loca.put(recensement,location);
                                    }

                                    //Intent pour notification
                                    IntentFilter intentFilter = new IntentFilter();
                                    intentFilter.addAction("Action Alerte");

                                    Context context = getApplicationContext();

                                    Notification.Builder builder;

                                    //Parcours de tous les localisations
                                    for(Map.Entry<RecensementVO, Location> location : loca.entrySet()) {

                                        //Parcours les alertes
                                        for(AlerteVO alerte : listeAlerte){
                                            String especeId = String.valueOf(alerte.getEspece().getId());

                                            //Espece si dans le rayon et si dans les alertes
                                            if(location.getValue().getProvider().equals(especeId) && loc.distanceTo(location.getValue()) < alerte.getRayon()){
                                                nbEspeceRayon++;

                                                //Stockage de la notification (Recensement, Espece, nb)
                                                HashMap<RecensementVO,EspeceVO> inter = new HashMap<>();
                                                inter.put(location.getKey(),alerte.getEspece());
                                                listeNotification.put(inter,nbEspeceRayon);

                                                loca.put(location.getKey(),location.getValue());
                                            }
                                        }
                                        nbEspeceRayon = 0;
                                    }

                                    //Parcours des notifications à effectués
                                    Set<Map.Entry<HashMap<RecensementVO, EspeceVO>, Integer>> set1 = listeNotification.entrySet();
                                    for (Map.Entry<HashMap<RecensementVO, EspeceVO>,Integer> entry1 : set1) {

                                        Integer nombreEspeceDansRayon = entry1.getValue();

                                        Map<RecensementVO, EspeceVO> map2 = entry1.getKey();
                                        Set<Map.Entry<RecensementVO, EspeceVO>> set2 = map2.entrySet();

                                        //Parcours des recensements à proximité des notifications
                                        for (Map.Entry<RecensementVO, EspeceVO> entry2 : set2) {

                                            //Test si l'alerte à deja été effectué
                                            if(! listeNotificationEffectue.contains(entry2.getKey())){

                                                //Map des notif à créer
                                                if(notif.containsKey(entry2.getValue())){
                                                    List list = notif.get(entry2.getValue());
                                                    list.add(entry2.getKey());
                                                    notif.put(entry2.getValue(), list);
                                                }else{
                                                    List list = new ArrayList<RecensementVO>();
                                                    list.add(entry2.getKey());
                                                    notif.put(entry2.getValue(), list);
                                                }

                                                //Ajout à la liste des notifs deja effectué
                                                listeNotificationEffectue.add(entry2.getKey());
                                            }

                                        }
                                    }


                                    int numeroNotif = 0; //numero de la notification

                                    //Notification qui sont créés
                                    for(Map.Entry<EspeceVO, List<RecensementVO>> noti : notif.entrySet()) {

                                        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.geoleaf);

                                        List list = noti.getValue();

                                        Intent myIntent = new Intent(getBaseContext(), UC00_AccueilActivity.class);
                                        myIntent.setAction(noti.getKey().getNomFrancais());

                                        RecensementsInstance.get().setNotifLargeData(noti.getKey(), noti.getValue());
                                        myIntent.putExtra("espece", noti.getKey());

                                        //myIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        myIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

                                        PendingIntent pendingIntent = PendingIntent.getActivity(getBaseContext(), 0, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                                        //Creation notification
                                        builder = new Notification.Builder(context)
                                                .setContentTitle("Alerte " + noti.getKey().getNomFrancais() + " !")
                                                .setContentText(list.size() + " " + noti.getKey().getNomFrancais() + " à proximité")
                                                .setContentIntent(pendingIntent)
                                                .setDefaults(Notification.DEFAULT_SOUND)
                                                .setAutoCancel(true)
                                                /*.setSmallIcon(android.R.drawable.ic_lock_lock);*/
                                                .setSmallIcon(R.drawable.ic_geoleaf)
                                                .setLargeIcon(bm);

                                        Notification notification = builder.build();

                                        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                        notificationManager.notify(numeroNotif, notification);

                                        numeroNotif++;
                                    }

                                    //suppression de la map des notifs
                                    notif.clear();

                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.d("MapDemoActivity", "Error trying to get last GPS location");
                                    e.printStackTrace();
                                }
                            });
                }
            }else {
                //Connexion internet perdu
            }
        }
    }

}
