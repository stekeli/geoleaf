package com.example.sifa.geoleaf.activities;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.example.sifa.geoleaf.services.RecensementService;
import com.example.sifa.geoleaf.utils.SessionManager;
import com.example.sifa.geoleaf.utils.TestConnection;
import com.example.sifa.geoleaf.vo.RecensementVO;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Kevin Wallois on 05/03/2018.
 */

public class NotificationAlerteInit extends Service{

    private Timer mTimer;
    private SessionManager session;

    //duree raffraichissement en seconde
    static final int DUREE_SECONDE = 3600;

    TimerTask timerTask = new TimerTask() {
        @Override
        public void run() {
            Log.e("NotificationAlerteInit","Run TimerTask");
            notifiy();
        }
    };

    @Override
    public void onDestroy() {
        try{
            mTimer.cancel();
            timerTask.cancel();
        }catch (Exception e){
            e.printStackTrace();
        }
        Intent intent = new Intent("com.example.sifa.geoleaf");
        intent.putExtra("intent","BodyIntent");
        sendBroadcast(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mTimer = new Timer();
        mTimer.schedule(timerTask, 2000, DUREE_SECONDE * 1000);
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try{

        }catch (Exception e){
            e.printStackTrace();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    //Creation des notifications des especes à proximités
    public void notifiy() throws SecurityException{

        session = new SessionManager(getApplicationContext());

        Log.i("Notif","Notifiy !!!");

        if(session.isLoggedIn()) {

            //Test si il y a une connexion internet
            if(TestConnection.isNetworkAvailable(this)) {
                //Recherche tous les recensements
                NotificationAlerte.listeRecensement.clear();
                List<RecensementVO> recensement = RecensementService.getRecensements();
                NotificationAlerte.listeRecensement.addAll(recensement);

            }else{
                //connexion internet perdu
            }

        }
    }


}
