package com.example.sifa.geoleaf.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import com.example.sifa.geoleaf.services.EspeceService;

/**
 * Created by Sifa on 22/02/2018.
 */

public class BitmapSerializer {

    public static Bitmap toBimap(String imageDataString){
        byte[] decodedString  = Base64.decode(imageDataString, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString , 0, decodedString.length);
        return decodedByte;
    }

}
