package com.example.sifa.geoleaf.activities;

import android.Manifest;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.example.sifa.geoleaf.R;
import com.example.sifa.geoleaf.adapters.CustomDetailWindowAdapter;
import com.example.sifa.geoleaf.adapters.CustomInfoWindowAdapter;
import com.example.sifa.geoleaf.cluster.ClusterMarker;
import com.example.sifa.geoleaf.cluster.CustomClusterRenderer;
import com.example.sifa.geoleaf.dialogs.BadgeDialog;
import com.example.sifa.geoleaf.dialogs.MapDialog;
import com.example.sifa.geoleaf.dialogs.RechercheDialog;
import com.example.sifa.geoleaf.services.BadgeService;
import com.example.sifa.geoleaf.services.RecensementService;
import com.example.sifa.geoleaf.utils.RecensementsInstance;
import com.example.sifa.geoleaf.utils.SessionManager;
import com.example.sifa.geoleaf.utils.TestConnection;
import com.example.sifa.geoleaf.vo.BadgeVO;
import com.example.sifa.geoleaf.vo.EspeceVO;
import com.example.sifa.geoleaf.vo.RecensementVO;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.maps.android.clustering.ClusterManager;

import java.util.ArrayList;
import java.util.List;

import static com.example.sifa.geoleaf.activities.UC02_RecenserActivity.LABELLE_NOUVEAU_RECEMSEMENT;
import static com.example.sifa.geoleaf.activities.UC13_LoadingActivity.RECENSEMENTS_KEY;
import static com.example.sifa.geoleaf.utils.vars.GEOLEAF_CYCLE;

public class UC00_AccueilActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback, View.OnClickListener, RechercheDialog.RechercheDialogListener {

    static final int LOGIN_REQUEST = 1;
    static final int RECENSER_REQUEST = 2;
    static final int PERMISSION_FINE_LOCATION_REQUEST = 3;
    static final int ALERTE_REQUEST = 4;
    static final int RECENSEMENT_REQUEST = 5;
    static final int BADGE_REQUEST = 6;

    private SessionManager session;

    private Intent intent;
    private NavigationView navigationView;
    private FloatingActionButton fab;
    private Toolbar toolbar;

    private MapFragment mapFragment;
    private GoogleMap map;

    private LocationManager locManager;

    private ClusterManager<ClusterMarker> mClusterManager;

    private List<RecensementVO> recensements;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(GEOLEAF_CYCLE, "CREATE ACTIVITY ACCUEIL");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.uc00_accueil_activity);

        session = new SessionManager(this); // Gestion de la session
        recensements = new ArrayList<>();

        locManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE); // Gestion de la localisation

        //Fragment de la map
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //Zoom carte
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //Bouton recenser espece
        fab = (FloatingActionButton) findViewById(R.id.fab);

        //listener widget
        fab.setOnClickListener(this);

        //Notification
        startService(new Intent(this, NotificationAlerteInit.class));
        startService(new Intent(this, NotificationAlerte.class));

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        //Menu lateral
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //Initialisation Bouton Connexion/Deconnexion
        if(session.isLoggedIn()) {
            navigationView.getMenu().findItem(R.id.nav_communicate).getSubMenu().findItem(R.id.nav_deconnexion).setTitle("Deconnexion");
        }else{
            navigationView.getMenu().findItem(R.id.nav_communicate).getSubMenu().findItem(R.id.nav_deconnexion).setTitle("Connexion");
        }

        //Met à jours la NavigationView qui contient les données utilisateurs (si connecté)
        updateNavigationView();

        //Tutoriel de bienvenue
        if(!session.isVisited(this.getLocalClassName())) {
            showTapTargets();
            session.setVisited(this.getLocalClassName(), true);
        }

        //Affiche la dialog d'alerte si necessaire
        showAlerteDialogIfNecessary(getIntent());
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mapFragment != null)
            mapFragment.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mapFragment != null)
            mapFragment.onResume();
    }

    //Affichage du tutoriel de bienvenue
    public void showTapTargets(){
        Log.i(GEOLEAF_CYCLE, "SHOW TAP TARGETS ACCUEIL");

        String titleFab = "Rencensez une espèce";
        String textFab = "Cliquez ici pour recenser une espèce et participer à l'apprentissage de notre nature";

        final String titleCon = "Oh, vous n'êtes pas connecté!";
        final String textCon = "Vous devez vous connecter pour recenser... Tout ce dont vous avez besoin se trouve ici :)";

        TapTargetView.showFor(this,
                TapTarget.forView(fab, titleFab, textFab).transparentTarget(true),
                new TapTargetView.Listener() {
                    @Override
                    public void onTargetClick(TapTargetView view) {
                        super.onTargetClick(view);
                        fab.performClick();
                        TapTargetView.showFor(UC00_AccueilActivity.this,
                                TapTarget.forView(toolbar.getChildAt(1), titleCon, textCon).transparentTarget(false),
                                new TapTargetView.Listener() {
                                    @Override
                                    public void onTargetClick(TapTargetView view) {
                                        super.onTargetClick(view);
                                        toolbar.getChildAt(1).performClick();
                                    }
                                });
                    }
                });

    }

    //Mise a jour du menu latéral si l'utilisateur est connecté
    public void updateNavigationView() {
        Log.i(GEOLEAF_CYCLE, "UPDATE NAVIGATION VIEW ACCUEIL");

        TextView navPrimary = navigationView.getHeaderView(0).findViewById(R.id.nav_primary);
        TextView navSecondary = navigationView.getHeaderView(0).findViewById(R.id.nav_secondary);
        if(session.isLoggedIn()){
            navPrimary.setText(session.getPseudo());
            navSecondary.setText(session.getEmail());
        } else {
            navPrimary.setText("Invité");
            navSecondary.setText("Fonctionnalités limitées");
        }
    }

    @Override
    public void onMapReady(final GoogleMap map) {
        Log.i(GEOLEAF_CYCLE, "ONMAPREADY ACCUEIL");
        // Authorise le zoom sur la carte
        map.getUiSettings().setZoomControlsEnabled(true);
        this.map = map;

        //Met à jours le contenu de la carte
        setUpClusterer();

        if(TestConnection.checkLocationPermission(this)) {
            //Si acces au GPS, zoom sur la position de l'utilisateur
            doLocation();
        }else {
            //Sinon, demande l'authorisation d'acces au GPS
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_FINE_LOCATION_REQUEST);
        }
    }

    //Met à jours le contenu de la carte
    private void setUpClusterer() {
        Log.i(GEOLEAF_CYCLE, "SETUPCLUSTURER ACCUEIL");

        // Initialisation manager avec le contexte et la carte
        mClusterManager = new ClusterManager(this, map);

        // Ajout des listener de click sur marqueur et fenetre de marqueur
        mClusterManager.getMarkerCollection().setOnInfoWindowAdapter(new CustomInfoWindowAdapter(this));
        mClusterManager.setOnClusterItemInfoWindowClickListener(new CustomDetailWindowAdapter(this));

        map.setInfoWindowAdapter(mClusterManager.getMarkerManager());
        map.setOnInfoWindowClickListener(mClusterManager.getMarkerManager());

        // Rendu du cluster (marker)
        final CustomClusterRenderer renderer = new CustomClusterRenderer(this, map, mClusterManager);
        mClusterManager.setRenderer(renderer);

        // Point the map's listeners at the listeners implemented by the cluster
        // manager.
        final CameraPosition[] mPreviousCameraPosition = {null};
        map.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                CameraPosition position = map.getCameraPosition();
                if(mPreviousCameraPosition[0] == null || mPreviousCameraPosition[0].zoom != position.zoom) {
                    mPreviousCameraPosition[0] = map.getCameraPosition();
                    mClusterManager.cluster();
                }
            }
        });
        map.setOnMarkerClickListener(mClusterManager);

        // Add cluster items (markers) to the cluster manager.
        placeRecensements(null);
    }

    //Place les recensements sur la carte
    //Si une espece est passée en paramètre, seul les recensements de cette espèce sont affichés
    public void placeRecensements(final EspeceVO espece) {
        Log.i(GEOLEAF_CYCLE, "PLACE LES RECENSEMENTS ACCUEIL");

        mClusterManager.clearItems();
        mClusterManager.cluster();

        new Thread(new Runnable() {
            @Override
            public void run() {
                int sync = getIntent().getIntExtra("recensements", -1);
                final List<RecensementVO> recensementsFromLoading = RecensementsInstance.get().getLargeData(sync);

                if(recensementsFromLoading == null || recensementsFromLoading.isEmpty()) {
                    if(recensements.isEmpty()) {
                        recensements = RecensementService.getRecensements();
                    }
                } else {
                    recensements = recensementsFromLoading;
                    getIntent().removeExtra(RECENSEMENTS_KEY);
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(espece != null) {
                            for(RecensementVO recensement : recensements) {
                                if(recensement.getEspece().equals(espece)) {
                                    placerRecensement(recensement);
                                }
                            }
                        } else {
                            for(RecensementVO recensement : recensements) {
                                placerRecensement(recensement);
                            }
                        }

                        mClusterManager.cluster();
                    }
                });
            }
        }).start();
    }

    //Place le recensement passé en parametre
    public void placerRecensement(RecensementVO recensement) {
        ClusterMarker offsetItem = new ClusterMarker(recensement);
        mClusterManager.addItem(offsetItem);
    }

    //Créé le menu d'options
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.i(GEOLEAF_CYCLE, "ONCREATEOPTIONMENU ACCUEIL");

        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.uc00__accueil, menu);

        return true;
    }

    //Zoom sur la position du l'utilisateur
    public void doLocation() throws SecurityException{
        Log.i(GEOLEAF_CYCLE, "DO LOCATION ACCUEIL");

        checkGpsEnabled();
        map.setMyLocationEnabled(true);

        FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // GPS location can be null if GPS is switched off
                        if (location != null) {
                            LatLng position = new LatLng(location.getLatitude(), location.getLongitude());
                            map.animateCamera(CameraUpdateFactory.newLatLngZoom(position, 12.0f), new GoogleMap.CancelableCallback() {
                                @Override
                                public void onFinish() {
                                }

                                @Override
                                public void onCancel() {
                                }
                            });
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("MapDemoActivity", "Error trying to get last GPS location");
                        e.printStackTrace();
                    }
                });
    }

    //Gestion du clic sur le menu de navigation
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (item.getItemId()){

            //menu Alertes
            case R.id.nav_alertes:
                //Test de connexion
                if(session.isLoggedIn()) {
                    intent = new Intent(UC00_AccueilActivity.this, UC01_ProfilActivity.class);
                    intent.putExtra(UC01_ProfilActivity.FRAGMENT_KEY, UC01_ProfilActivity.ALERTES_FRAGMENT_ID);
                    startActivityForResult(intent, BADGE_REQUEST);
                } else {
                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Connectez-vous.", Snackbar.LENGTH_LONG);
                    snackbar.setAction("Se connecter", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            launchIntentFor(UC07_ConnexionActivity.class);
                        }
                    });
                    snackbar.show();
                }
                break;

            //menu Deconnexion
            case R.id.nav_deconnexion:
                //Test de connexion
                if(session.isLoggedIn()) {
                    logout();
                }else{
                    //navigationView.getMenu().findItem(R.id.nav_communicate).getSubMenu().findItem(R.id.nav_deconnexion).setTitle("Deconnexion");
                    Intent intentForConnexion = new Intent(UC00_AccueilActivity.this, UC07_ConnexionActivity.class);
                    startActivityForResult(intentForConnexion, LOGIN_REQUEST);
                }
                break;

            //menu Recensements
            case R.id.nav_recensements:
                //Test de connexion
                if(session.isLoggedIn()) {
                    intent = new Intent(UC00_AccueilActivity.this, UC01_ProfilActivity.class);
                    intent.putExtra(UC01_ProfilActivity.FRAGMENT_KEY, UC01_ProfilActivity.RECENSEMENTS_FRAGMENT_ID);
                    startActivityForResult(intent, RECENSEMENT_REQUEST);
                } else {
                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Connectez-vous.", Snackbar.LENGTH_LONG);
                    snackbar.setAction("Se connecter", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            launchIntentFor(UC07_ConnexionActivity.class);
                        }
                    });
                    snackbar.show();
                }
                break;


            //menu badge
            case R.id.nav_badges:
                //Test de connexion
                if(session.isLoggedIn()) {
                    intent = new Intent(UC00_AccueilActivity.this, UC01_ProfilActivity.class);
                    intent.putExtra(UC01_ProfilActivity.FRAGMENT_KEY, UC01_ProfilActivity.BADGES_FRAGMENT_ID);
                    startActivityForResult(intent, BADGE_REQUEST);
                }else {
                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Connectez-vous.", Snackbar.LENGTH_LONG);
                    snackbar.setAction("Se connecter", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            launchIntentFor(UC07_ConnexionActivity.class);
                        }
                    });
                    snackbar.show();
                }
                break;

            //menu identification
            case R.id.nav_identification:
                Intent intentForIdentification = new Intent(UC00_AccueilActivity.this, UC14_IdentificationActivity.class);
                startActivity(intentForIdentification);
                break;

            //menu Profil
            case R.id.nav_profil:
                //Test de connexion
                if(session.isLoggedIn()) {
                    Intent intentForProfil = new Intent(UC00_AccueilActivity.this, UC01_ProfilActivity.class);
                    intentForProfil.putExtra(UC01_ProfilActivity.FRAGMENT_KEY, UC01_ProfilActivity.PROFIL_FRAGMENT_ID);
                    startActivity(intentForProfil);
                }else{
                    Intent intentForConnexion = new Intent(UC00_AccueilActivity.this, UC07_ConnexionActivity.class);
                    startActivityForResult(intentForConnexion, LOGIN_REQUEST);
                }
                break;

            //menu historique
            case R.id.nav_historique:
                intent = new Intent(UC00_AccueilActivity.this, UC11_HistoriqueActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void logout(){
        navigationView.getMenu().findItem(R.id.nav_communicate).getSubMenu().findItem(R.id.nav_deconnexion).setTitle("Connexion");
        session.logoutUser();
        updateNavigationView();
    }

    public void login() {
        navigationView.getMenu().findItem(R.id.nav_communicate).getSubMenu().findItem(R.id.nav_deconnexion).setTitle("Deconnexion");
        updateNavigationView();
    }

    //Listener widgets
    @Override
    public void onClick(View view) {

        switch (view.getId()){

            //Bouton ajout Recensement
            case R.id.fab:
                if(session.isLoggedIn()) {
                    Intent intentForRecenser = new Intent(UC00_AccueilActivity.this, UC02_RecenserActivity.class);
                    startActivityForResult(intentForRecenser, RECENSER_REQUEST);
                } else {
                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Pour recenser, connectez-vous.", Snackbar.LENGTH_LONG);
                    snackbar.setAction("Se connecter", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            launchIntentFor(UC07_ConnexionActivity.class);
                        }
                    });
                    snackbar.show();
                }
                break;

            default:
                break;

        }
    }

    //En cas d'intent sur l'accueil alors que l'accueil est déjà en cours d'execution
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        //Affiche la dialogue l'alerte si necessaire
        showAlerteDialogIfNecessary(intent);
    }

    //Dialogue affichage clic notification alerte
    public void showAlerteDialogIfNecessary(Intent intent){
        EspeceVO espece = (EspeceVO) intent.getSerializableExtra("espece");
        if(espece != null) {
            List<RecensementVO> recensementVOs = RecensementsInstance.get().getNotifLargeData(espece);

            android.support.v4.app.DialogFragment dialog = MapDialog.newInstance(espece, recensementVOs);
            dialog.show(getSupportFragmentManager(), "MapDialog");
        }
    }

    //Gestion du clic sur le menu d'options
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //Icone connexion/profil
        if (id == R.id.action_login) {
            if (session.isLoggedIn()) {
                Intent intentForProfil = new Intent(UC00_AccueilActivity.this, UC01_ProfilActivity.class);
                intentForProfil.putExtra(UC01_ProfilActivity.FRAGMENT_KEY, UC01_ProfilActivity.PROFIL_FRAGMENT_ID);
                startActivity(intentForProfil);
            } else {
                launchIntentFor(UC07_ConnexionActivity.class);
            }
        }

        //Icone recherche un lieu/espece
        if (id == R.id.action_recherche) {
            DialogFragment dialog = new RechercheDialog();
            dialog.show(getFragmentManager(), "RechercheDialog");
        }

        return super.onOptionsItemSelected(item);
    }

    //Appelé en cas de validation de recherche
    @Override
    public void onDialogPositiveClick(DialogFragment dialog, EspeceVO especeVO, Place place) {
        placeRecensements(especeVO);
        if(place != null) {
            map.animateCamera(CameraUpdateFactory.newLatLng(place.getLatLng()));
        }
    }

    //Appelé en cas d'annulation de recherche
    @Override
    public void onDialogNegativeClick(DialogFragment dialog, EspeceVO especeVO, Place place) {}

    //Reponse retour intent
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        //Retour Intent Connexion
        if(requestCode == LOGIN_REQUEST && resultCode == RESULT_OK) {
            login();
        }

        //Retour Intent recenser espece
        if (requestCode == RECENSER_REQUEST && resultCode == RESULT_OK) {
            final RecensementVO recensement = (RecensementVO) data.getSerializableExtra(LABELLE_NOUVEAU_RECEMSEMENT);
            recensements.add(recensement);
            placeRecensements(null);
            mClusterManager.cluster();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    //Test si badge gagné
                    final BadgeVO badge = BadgeService.isWinningABadge(UC00_AccueilActivity.this);

                    if(badge != null) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                BadgeDialog badgeDialog = BadgeDialog.newInstance(badge);
                                badgeDialog.show(getFragmentManager(), "BadgeDialog");
                            }
                        });
                    }
                }
            }).start();
        }
    }

    //Permission GPS
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_FINE_LOCATION_REQUEST: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    doLocation();
                }
                break;
            }
        }
    }

    //Bouton précédent en haut à gauche
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    //Template lancement activité login
    public void launchIntentFor(Class classe){
        Intent intentForLogin = new Intent(UC00_AccueilActivity.this, classe);
        startActivityForResult(intentForLogin, LOGIN_REQUEST);
    }

    //Verification GPS activé
    public void checkGpsEnabled(){
        if(!locManager.isProviderEnabled( LocationManager.GPS_PROVIDER )){
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "GPS système inactif.", Snackbar.LENGTH_LONG);
            snackbar.setAction("Activer", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                }
            });
            snackbar.show();
        }
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return super.dispatchTouchEvent(ev);
    }
}
