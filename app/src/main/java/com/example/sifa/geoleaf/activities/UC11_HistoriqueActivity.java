package com.example.sifa.geoleaf.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.example.sifa.geoleaf.R;
import com.example.sifa.geoleaf.adapters.CustomDetailWindowAdapter;
import com.example.sifa.geoleaf.adapters.CustomInfoWindowAdapter;
import com.example.sifa.geoleaf.cluster.ClusterMarker;
import com.example.sifa.geoleaf.cluster.CustomClusterRenderer;
import com.example.sifa.geoleaf.services.RecensementService;
import com.example.sifa.geoleaf.utils.TestConnection;
import com.example.sifa.geoleaf.vo.EspeceVO;
import com.example.sifa.geoleaf.vo.RecensementVO;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.maps.android.clustering.ClusterManager;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static android.R.attr.targetSdkVersion;

public class UC11_HistoriqueActivity extends AppCompatActivity {

    static final int PERMISSION_FINE_LOCATION_REQUEST = 1;

    int map1_initial_year = 2017;
    int map2_initial_year = 2018;

    private GoogleMap map1, map2;
    private DiscreteSeekBar seekBar1, seekBar2;
    private TextView year1, year2;

    private ClusterManager<ClusterMarker> mClusterManager1, mClusterManager2;

    private List<RecensementVO> recensements = new ArrayList<>();

    MapFragment mapFragment1;
    MapFragment mapFragment2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.uc11_activity_historique);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mapFragment1 = (MapFragment) getFragmentManager().findFragmentById(R.id.map1);
        mapFragment2 = (MapFragment) getFragmentManager().findFragmentById(R.id.map2);

        seekBar1 = (DiscreteSeekBar) findViewById(R.id.seekbar1);
        seekBar2 = (DiscreteSeekBar) findViewById(R.id.seekbar2);
        seekBar1.setProgress(map1_initial_year);
        seekBar2.setProgress(map2_initial_year);

        year1 = (TextView) findViewById(R.id.year1);
        year2 = (TextView) findViewById(R.id.year2);
        year1.setText(""+seekBar1.getProgress());
        year2.setText(""+seekBar2.getProgress());

        new Thread(new Runnable() {
            @Override
            public void run() {
                recensements = RecensementService.getRecensements();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mapFragment1.getMapAsync(onMapReadyCallback1());
                        mapFragment2.getMapAsync(onMapReadyCallback2());
                    }
                });
            }

        }).start();

        addListeners();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mapFragment1 != null) mapFragment1.onPause();
        if(mapFragment2 != null) mapFragment2.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mapFragment1 != null) mapFragment1.onResume();
        if(mapFragment2 != null) mapFragment2.onResume();
    }

    //Met à jours le contenu de la carte
    private void setUpClusterer(final ClusterManager mClusterManager, final GoogleMap map, final Integer year) {
        // Ajout des listener de click sur marqueur et fenetre de marqueur
        mClusterManager.getMarkerCollection().setOnInfoWindowAdapter(new CustomInfoWindowAdapter(this));
        mClusterManager.setOnClusterItemInfoWindowClickListener(new CustomDetailWindowAdapter(this));

        map.setInfoWindowAdapter(mClusterManager.getMarkerManager());
        map.setOnInfoWindowClickListener(mClusterManager.getMarkerManager());

        // Rendu du cluster (marker)
        final CustomClusterRenderer renderer = new CustomClusterRenderer(this, map, mClusterManager);
        mClusterManager.setRenderer(renderer);

        // Point the map's listeners at the listeners implemented by the cluster
        // manager.
        final CameraPosition[] mPreviousCameraPosition = {null};
        map.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                CameraPosition position = map.getCameraPosition();
                if(mPreviousCameraPosition[0] == null || mPreviousCameraPosition[0].zoom != position.zoom) {
                    mPreviousCameraPosition[0] = map.getCameraPosition();
                    mClusterManager.cluster();
                }
            }
        });
        map.setOnMarkerClickListener(mClusterManager);

        // Add cluster items (markers) to the cluster manager.
        updateRecensements(mClusterManager, null,  year);
    }


    public void updateRecensements(final ClusterManager mClusterManager, final EspeceVO espece, final Integer year) {
        mClusterManager.clearItems();
        if(year == null) {
            for(RecensementVO recensement : recensements) {
                placerRecensement(mClusterManager, recensement);
            }
        } else {
            for(RecensementVO recensement : recensements) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(recensement.getDateRecensement());
                if(cal.get(Calendar.YEAR) == year) {
                    placerRecensement(mClusterManager, recensement);
                }
            }
        }
        mClusterManager.cluster();
    }

    //Place le recensement dans la clusterManager passé en parametre
    public void placerRecensement(ClusterManager mClusterManager, RecensementVO recensement) {
        ClusterMarker offsetItem = new ClusterMarker(recensement);
        mClusterManager.addItem(offsetItem);
    }

    //Ajoute les listeners d'actions
    public void addListeners() {
        seekBar1.setOnProgressChangeListener(new DiscreteSeekBar.OnProgressChangeListener() {
            @Override
            public void onProgressChanged(DiscreteSeekBar seekBar, int value, boolean fromUser) {
                year1.setText(""+value);
                updateRecensements(mClusterManager1, null, value);
            }

            @Override
            public void onStartTrackingTouch(DiscreteSeekBar seekBar) { }

            @Override
            public void onStopTrackingTouch(DiscreteSeekBar seekBar) { }
        });

        seekBar2.setOnProgressChangeListener(new DiscreteSeekBar.OnProgressChangeListener() {
            @Override
            public void onProgressChanged(DiscreteSeekBar seekBar, int value, boolean fromUser) {
                year2.setText(""+value);
                updateRecensements(mClusterManager2, null, value);
            }

            @Override
            public void onStartTrackingTouch(DiscreteSeekBar seekBar) { }

            @Override
            public void onStopTrackingTouch(DiscreteSeekBar seekBar) { }
        });

    }

    //Map1 chargée
    public OnMapReadyCallback onMapReadyCallback1(){
        return new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                map1 = googleMap;
                map1.getUiSettings().setZoomControlsEnabled(true);
                map1.getUiSettings().setMapToolbarEnabled(false);

                doLocation(map1);

                mClusterManager1 = new ClusterManager(getApplicationContext(), map1);
                setUpClusterer(mClusterManager1, map1, map1_initial_year);
            }
        };
    }

    //Map2 chargée
    public OnMapReadyCallback onMapReadyCallback2(){
        return new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                map2 = googleMap;
                map2.getUiSettings().setZoomControlsEnabled(true);
                map2.getUiSettings().setMapToolbarEnabled(false);

                doLocation(map2);

                //Quand map2 déplacée, map1 suit map2
                map2.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
                    @Override
                    public void onCameraMove() {
                        map1.moveCamera(CameraUpdateFactory.newCameraPosition(map2.getCameraPosition()));
                    }
                });

                mClusterManager2 = new ClusterManager(getApplicationContext(), map2);
                setUpClusterer(mClusterManager2, map2, map2_initial_year);
            }
        };
    }

    //La camera de la map est déplacée et zoomée sur la position de l'utilisateur
    public void doLocation(final GoogleMap map){
        if(TestConnection.checkLocationPermission(this)) {
            FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getApplicationContext());
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // GPS location can be null if GPS is switched off
                            if (location != null) {
                                LatLng position = new LatLng(location.getLatitude(), location.getLongitude());
                                map.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 12.0f));
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.d("MapDemoActivity", "Error trying to get last GPS location");
                            e.printStackTrace();
                        }
                    });
        }else {
            ActivityCompat.requestPermissions(UC11_HistoriqueActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_FINE_LOCATION_REQUEST);
        }
    }

    //Bouton précédent en haut à gauche
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}
