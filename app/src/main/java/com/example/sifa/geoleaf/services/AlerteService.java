package com.example.sifa.geoleaf.services;

import android.content.Context;

import com.example.sifa.geoleaf.utils.HttpRequest;
import com.example.sifa.geoleaf.vo.AlerteVO;
import com.example.sifa.geoleaf.vo.UtilisateurVO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static com.example.sifa.geoleaf.utils.vars.BASE_URL;

/**
 * Created by Kevin Wallois on 07/02/2018.
 */

public class AlerteService {


    private static final Gson gson = new GsonBuilder().setDateFormat("yyyy-mm-dd").create();

    //Ajoute une nouvelle alerte
    public static void addAlerte(AlerteVO alerte, Context context) {
        alerte.setUtilisateur(UtilisateurService.getCurrentUtilisateur(context));
        HttpRequest.post(BASE_URL + "/alerte/add").acceptJson().contentType("application/json").send(gson.toJson(alerte)).code();
    }

    //Liste toutes les alertes
    public static List<AlerteVO> getAlertesByUser(UtilisateurVO utilisateur) {
        HttpRequest request = HttpRequest.get(BASE_URL + "/alertes/user", true, "utilisateur", gson.toJson(utilisateur));
        Type listAVO = new TypeToken<ArrayList<AlerteVO>>(){}.getType();
        List<AlerteVO> listeAlertes = gson.fromJson(request.body(), listAVO);
        return listeAlertes;
    }

    //Modifie une alerte
    public static void updateAlerte(AlerteVO alerte){
        HttpRequest.post(BASE_URL + "/alerte/update").acceptJson().contentType("application/json").send(gson.toJson(alerte)).code();
    }

    //Liste toutes les alertes actif
    public static List<AlerteVO> getAlertesByUserActif(UtilisateurVO utilisateur) {
        HttpRequest request = HttpRequest.get(BASE_URL + "/alertes/user/actif", true, "utilisateur", gson.toJson(utilisateur));
        Type listAVO = new TypeToken<ArrayList<AlerteVO>>() {}.getType();
        List<AlerteVO> listeAlertes = gson.fromJson(request.body(), listAVO);
        return listeAlertes;
    }

    //Suppression d'une alerte
    public static void deleteAlerte(AlerteVO alerte){
        HttpRequest.post(BASE_URL + "/alerte/delete").acceptJson().contentType("application/json").send(gson.toJson(alerte)).code();
    }
}
