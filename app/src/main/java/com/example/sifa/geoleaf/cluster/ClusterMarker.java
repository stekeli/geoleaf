package com.example.sifa.geoleaf.cluster;

import com.example.sifa.geoleaf.vo.RecensementVO;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

/**
 * Created by Sifa on 23/02/2018.
 */

public class ClusterMarker implements ClusterItem {
    private final LatLng mPosition;
    private final RecensementVO recensement;

    public ClusterMarker(RecensementVO recensement) {
        this.mPosition = new LatLng(recensement.getGpsLatitude(), recensement.getGpsLongitude());
        this.recensement = recensement;
    }

    @Override
    public LatLng getPosition() {
        return mPosition;
    }

    public RecensementVO getRecensement(){
        return recensement;
    }
}
