package com.example.sifa.geoleaf.services;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.example.sifa.geoleaf.R;
import com.example.sifa.geoleaf.dialogs.BadgeDialog;
import com.example.sifa.geoleaf.utils.BitmapSerializer;
import com.example.sifa.geoleaf.utils.Cache;
import com.example.sifa.geoleaf.utils.HttpRequest;
import com.example.sifa.geoleaf.vo.AlerteVO;
import com.example.sifa.geoleaf.vo.BadgeVO;
import com.example.sifa.geoleaf.vo.EspeceVO;
import com.example.sifa.geoleaf.vo.UtilisateurBadgeVO;
import com.example.sifa.geoleaf.vo.UtilisateurVO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static com.example.sifa.geoleaf.utils.vars.BASE_URL;

/**
 * Created by Sifa on 07/03/2018.
 */

public class BadgeService {

    private static final Gson gson = new GsonBuilder().setDateFormat("yyyy-mm-dd").create();

    public static List<BadgeVO> getBadges() {
        HttpRequest request = HttpRequest.get(BASE_URL + "/badges", true);
        Type listBVO = new TypeToken<ArrayList<BadgeVO>>(){}.getType();
        List<BadgeVO> badges = gson.fromJson(request.body(), listBVO);
        return badges;
    }

    public static Bitmap getImage(BadgeVO badge, Context context) {
        Bitmap bitmap = Cache.get("badge_image"+badge.getId());

        if(bitmap == null) {
            // Si l'image n'est pas dans le cache, on va la chercher
            int drawableID = context.getResources().getIdentifier(badge.getImagePath(), "drawable", context.getPackageName());
            bitmap = BitmapFactory.decodeResource(context.getResources(), drawableID);
            if(bitmap == null) {
                // Si le serveur ne trouve pas l'image, on met l'image d'arbre inconnu
                bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.badge_inconnu);
            } else {
                // Si le serveur trouve l'image, on la stock dans le cache
                Cache.put(badge.getId()+"", bitmap);
            }
        }

        // On retourne l'image
        return bitmap;
    }


    public static List<UtilisateurBadgeVO> getUtilisateurBadgesForUtilisateur(UtilisateurVO utilisateurVO) {
        HttpRequest request = HttpRequest.get(BASE_URL + "/badges/utilisateur", true, "utilisateur", gson.toJson(utilisateurVO));
        Type listBVO = new TypeToken<ArrayList<UtilisateurBadgeVO>>(){}.getType();
        List<UtilisateurBadgeVO> listeBadges = gson.fromJson(request.body(), listBVO);
        return listeBadges;
    }

    public static List<BadgeVO> getBadgesForUtilisateur(UtilisateurVO utilisateurVO) {
        List<UtilisateurBadgeVO> utilisateurBadgeVOs = getUtilisateurBadgesForUtilisateur(utilisateurVO);
        List<BadgeVO> listeBadges = new ArrayList<>();
        for(UtilisateurBadgeVO utilisateurBadgeVO : utilisateurBadgeVOs) {
            listeBadges.add(utilisateurBadgeVO.getBadge());
        }
        return listeBadges;
    }

    public static void addBadgeForUtilisateur(UtilisateurVO utilisateurVO, BadgeVO badgeVO){
        UtilisateurBadgeVO utilisateurBadgeVO = new UtilisateurBadgeVO();
        utilisateurBadgeVO.setUtilisateur(utilisateurVO);
        utilisateurBadgeVO.setBadge(badgeVO);
        HttpRequest.post(BASE_URL + "/badge/utilisateur/add").acceptJson().contentType("application/json").send(gson.toJson(utilisateurBadgeVO)).code();
    }

    public static BadgeVO isWinningABadge(Activity activity){
        UtilisateurVO utilisateur = UtilisateurService.getCurrentUtilisateur(activity);
        List<BadgeVO> badges = BadgeService.getBadges();
        List<BadgeVO> user_badges = getBadgesForUtilisateur(utilisateur);

        Integer user_recensements_number = RecensementService.countRecensementsByUser(utilisateur);

        BadgeVO earnedBadge = null;

        for(BadgeVO badge: badges) {
            if(user_recensements_number >= badge.getNombre()) {
                if(!user_badges.contains(badge)) {
                    addBadgeForUtilisateur(utilisateur, badge);
                    earnedBadge = badge;
                }
            }
        }
        return earnedBadge;

    }

}
