package com.example.sifa.geoleaf.vo;

import java.io.Serializable;

/**
 * Created by Kevin Wallois on 26/01/2018.
 */

public class EspeceVO implements Serializable {
    private int id;
    private String nomFrancais;
    private String nomScientifique;
    private String description;
    private String climat;
    private String imagePath;
    private String lien;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomFrancais() {
        return nomFrancais;
    }

    public void setNomFrancais(String nomFrancais) {
        this.nomFrancais = nomFrancais;
    }

    public String getNomScientifique() {
        return nomScientifique;
    }

    public void setNomScientifique(String nomScientifique) {
        this.nomScientifique = nomScientifique;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getClimat() {
        return climat;
    }

    public void setClimat(String climat) {
        this.climat = climat;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    @Override
    public String toString() {
        return nomFrancais;
    }

    public String getLien() {
        return lien;
    }

    public void setLien(String lien) {
        this.lien = lien;
    }

    @Override
    public boolean equals(Object obj) {
        EspeceVO espece = (EspeceVO) obj;
        return (espece.getId() == this.getId());
    }

    @Override
    public int hashCode() {
        return this.id;
    }
}
